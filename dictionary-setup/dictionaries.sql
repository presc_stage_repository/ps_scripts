
Insert into PS_ANON_TYP (ANON_TYP_CD) values ('BUREAU');
Insert into PS_ANON_TYP (ANON_TYP_CD) values ('SEQUENCE');

Insert into PS_CTRY (CTRY_ISO_CD,CTRY_NM) values ('GBR','United Kingdom');
Insert into PS_CTRY (CTRY_ISO_CD,CTRY_NM) values ('IRL','Ireland');

Insert into PS_ORG (CTRY_ISO_CD,ORG_NM,EFF_FR_DT,EFF_TO_DT,ISRTED_DT,ORG_ID,ORG_SHORT_NM,ORG_DESC) values ('GBR','Bureau',null,null,null,'1','Bureau','Trusted Third Party - Bureau');
Insert into PS_ORG (CTRY_ISO_CD,ORG_NM,EFF_FR_DT,EFF_TO_DT,ISRTED_DT,ORG_ID,ORG_SHORT_NM,ORG_DESC) values ('IRL','Lloyds',null,null,null,'2','LLY','Lloyds pharamcy');
Insert into PS_ORG (CTRY_ISO_CD,ORG_NM,EFF_FR_DT,EFF_TO_DT,ISRTED_DT,ORG_ID,ORG_SHORT_NM,ORG_DESC) values ('IRL','Mclernons',null,null,null,'3','MCL','Mclernons');
Insert into PS_ORG (CTRY_ISO_CD,ORG_NM,EFF_FR_DT,EFF_TO_DT,ISRTED_DT,ORG_ID,ORG_SHORT_NM,ORG_DESC) values ('IRL','TOUCHSTORE',null,null,null,'5','TCH','TOUCHSTORE');
Insert into PS_ORG (CTRY_ISO_CD,ORG_NM,EFF_FR_DT,EFF_TO_DT,ISRTED_DT,ORG_ID,ORG_SHORT_NM,ORG_DESC) values ('IRL','UNKNOWN',null,null,null,'6','UNKNOWN','UNKNOWN ORGANIZATION');

Insert into PS_ORG_ROLE_TYP (ORG_ROLE_TYP_CD,ORG_ROLE_TYP_SHORT_NM,ORG_ROLE_TYP_NM,ORG_ROLE_TYP_DESC,EFF_FR_DT,EFF_TO_DT,ISRTED_DT) values ('SUPP','Data supplier','Data supplier','Data supplier',null,null,null);

Insert into PS_ORG_ROLE (ORG_ROLE_CD,ORG_ROLE_NM,ORG_ROLE_SHORT_NM,ORG_ROLE_DESC,ORG_ROLE_ID,ORG_ROLE_TYP_CD,ORG_ID) values ('SUPP_Bureau','Data supplier','Data supplier','Data supplier','1','SUPP','1');
Insert into PS_ORG_ROLE (ORG_ROLE_CD,ORG_ROLE_NM,ORG_ROLE_SHORT_NM,ORG_ROLE_DESC,ORG_ROLE_ID,ORG_ROLE_TYP_CD,ORG_ID) values ('SUPP_LLY','Data supplier','Data supplier','Data supplier','2','SUPP','2');
Insert into PS_ORG_ROLE (ORG_ROLE_CD,ORG_ROLE_NM,ORG_ROLE_SHORT_NM,ORG_ROLE_DESC,ORG_ROLE_ID,ORG_ROLE_TYP_CD,ORG_ID) values ('SUPP_MCL','Data supplier','Data supplier','Data supplier','3','SUPP','3');

Insert into PS_RXER_TYP (RXER_TYP_CD,RXER_TYP_SHORT_NM,RXER_TYP_DESC_TXT) values ('D','Dentist','Dentist');
Insert into PS_RXER_TYP (RXER_TYP_CD,RXER_TYP_SHORT_NM,RXER_TYP_DESC_TXT) values ('V','Vet','Vet');
Insert into PS_RXER_TYP (RXER_TYP_CD,RXER_TYP_SHORT_NM,RXER_TYP_DESC_TXT) values ('N','Nurse','Nurse');
Insert into PS_RXER_TYP (RXER_TYP_CD,RXER_TYP_SHORT_NM,RXER_TYP_DESC_TXT) values ('O','Other','Other');
Insert into PS_RXER_TYP (RXER_TYP_CD,RXER_TYP_SHORT_NM,RXER_TYP_DESC_TXT) values ('G','GP','GP Data');
Insert into PS_RXER_TYP (RXER_TYP_CD,RXER_TYP_SHORT_NM,RXER_TYP_DESC_TXT) values ('H','HOSP','Hospital Doc');



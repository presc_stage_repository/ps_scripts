
  CREATE OR REPLACE VIEW "PS_ADMIN"."PS_FILE_PROCESSING" ("id", "dcpOrganisationId", "dcpOrganisationName", "countryIsoCode", "country", "inputFileName", "inputFileProcessedDate", "outputFileName", "outputFileRecordCount", "outputFileProcessedDate", "transactionDate", "imsSupplierIdentifier", "externalSupplierIdentifier", "fileStatus", "sourceFileIdentifier", "inputFileRecordCount", "updateCount", "deleteCount", "duplicateCount", "ignoreCount", "validCount", "invalidCount") AS 
  (SELECT DISTINCT
        standard_hash(TO_CHAR (PFD.RX_DSPNSD_DT, 'mmddyyyyhhmiss')||','||
                  TO_CHAR (PF.FILE_PROC_DT, 'mmddyyyyhhmiss')||','||PF.INPUT_FILE_NM,'MD5') AS "id",
        PO.ORG_ID AS "dcpOrganisationId",
        PO.ORG_NM AS "dcpOrganisationName",
        PO.CTRY_ISO_CD AS "countryIsoCode",
        PC.CTRY_NM AS "country",
        PF.INPUT_FILE_NM AS "inputFileName",
        PF.FILE_PROC_DT AS "inputFileProcessedDate",
        PF.FILE_NM AS "outputFileName",
        PFD.REC_CNT
        - (  NVL (PFD.DUPE_CNT, 0)
           + NVL (PFD.IGNR_CNT, 0)) AS "outputFileRecordCount",
        PF.FILE_PROC_DT AS "outputFileProcessedDate",
        PFD.RX_DSPNSD_DT AS "transactionDate",
        PP.EXTRNL_PHARMACY_ID AS "imsSupplierIdentifier",
        PP.INTR_PHARMACY_ID AS "externalSupplierIdentifier",
        PFP.FILE_STATUS AS "fileStatus",
        PF.SRC_FILE_ID AS "sourceFileIdentifier",
        PFD.REC_CNT AS "inputFileRecordCount",
        PFD.UPDT_CNT AS "updateCount",
        NVL (PFD.DEL_CNT, 0) AS "deleteCount",
        PFD.DUPE_CNT AS "duplicateCount",
        NVL (PFD.IGNR_CNT, 0) AS "ignoreCount",
          PFD.REC_CNT
        - (  NVL (PFD.DUPE_CNT, 0)
           + NVL (PFD.IGNR_CNT, 0))
           AS "validCount",
            NVL (PFD.DUPE_CNT, 0)
           + NVL (PFD.IGNR_CNT, 0)
           AS "invalidCount"
   FROM PS_ORG PO
        INNER JOIN PS_CTRY PC ON PO.CTRY_ISO_CD = PC.CTRY_ISO_CD
        INNER JOIN PS_FILE PF ON PO.ORG_ID = PF.ORG_ID
        INNER JOIN PS_FILE_PROC_DETLS PFP ON PFP.SRC_FILE_ID = PF.SRC_FILE_ID
        INNER JOIN PS_PHARMACY PP
           ON PFP.SURRO_PHARMACY_ID = PP.SURRO_PHARMACY_ID
        INNER JOIN PS_FILE_DT_PROC_DETLS PFD
           ON PFD.SRC_FILE_ID = PF.SRC_FILE_ID);
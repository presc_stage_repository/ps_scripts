
CREATE TABLE PS_ADMIN.PS_ANON_RXER
(
	ANON_RXER_ID         INTEGER NOT NULL ,
	RXER_TYP_CD          VARCHAR2(10) NULL ,
	ANON_TYP_CD          VARCHAR2(10) NULL ,
	SRC_FILE_ID          INTEGER NULL ,
	EXTRNL_ID            VARCHAR2(20) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_ANON_RXER IS 'This entity represents anonymized Prescriber that is used to link anonymized fact data  with Prescriber info used for reporting purposes. It allows to report on Location or Speciality level without the risk of identifing any particular Prescirber. ';

CREATE TABLE PS_ADMIN.PS_ANON_RXER_BAS_STG
(
	SHOP_ID              INTEGER NULL ,
	EXTRNL_SHOP_ID       CHAR(18) NULL ,
	SYS_CD               VARCHAR2(20) NULL ,
	SEQ_ID               INTEGER NULL ,
	RXER_TYP_CD          VARCHAR2(10) NULL ,
	LOC_ADDR_LINE_1_TXT  varchar2(512) NULL ,
	LOC_ADDR_LINE_2_TXT  VARCHAR2(512) NULL ,
	LOC_ADDR_LINE_3_TXT  VARCHAR2(512) NULL ,
	LOC_ADDR_LINE_4_TXT  VARCHAR2(512) NULL ,
	PSTL_CD              VARCHAR2(50) NULL ,
	CTRY_NM              VARCHAR2(64) NOT NULL ,
	MINI_BRICK           VARCHAR2(20) NULL ,
	EFF_FR_DT            DATE NULL ,
	EFF_TO_DT            DATE NULL 
);

COMMENT ON COLUMN PS_ADMIN.PS_ANON_RXER_BAS_STG.LOC_ADDR_LINE_1_TXT IS 'This refers to the first element of an atomicised address or could contain the entire address if an atomicised address model is not used.';

COMMENT ON COLUMN PS_ADMIN.PS_ANON_RXER_BAS_STG.LOC_ADDR_LINE_2_TXT IS 'This refers to the second element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_ANON_RXER_BAS_STG.LOC_ADDR_LINE_3_TXT IS 'This refers to the third element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_ANON_RXER_BAS_STG.LOC_ADDR_LINE_4_TXT IS 'This refers to the fourth element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_ANON_RXER_BAS_STG.PSTL_CD IS 'This is records the Post Code for the Geo Location. Another commonly used term for this zone shortcut is ''Zip Code''.';

CREATE TABLE PS_ADMIN.PS_ANON_RXER_GEO_LOC_STG
(
	ANON_RXER_ID         INTEGER NULL ,
	LOC_MINI_BRICK       VARCHAR2(20) NULL ,
	RXER_SPCL            VARCHAR2(20) NULL ,
	RXER_PVT_FLG         VARCHAR2(1) NULL ,
	RXER_QUALFCTN_YR     VARCHAR2(20) NULL ,
	RXER_DSPNSING_FLG    VARCHAR2(1) NULL ,
	RXER_FUNDHLDER_FLG   VARCHAR2(1) NULL ,
	RXER_CONSNT_FLG      VARCHAR2(10) NULL ,
	RXER_GENDER          VARCHAR2(20) NULL 
);

CREATE TABLE PS_ADMIN.PS_ANON_RXER_LOC_STG
(
	RXER_ID              VARCHAR2(150) NULL ,
	ANON_RXER_ID         INTEGER NULL ,
	LOC_MINI_BRICK       VARCHAR2(20) NULL ,
	SYS_CD               VARCHAR2(20) NULL ,
	SEQ_ID               INTEGER NULL ,
	EXTRNL_LOC_ID        INTEGER NULL ,
	PSTL_CD              VARCHAR2(10) NULL ,
	RXER_TYP             VARCHAR2(1) NULL ,
	LOC_TYP              VARCHAR2(1) NULL ,
	SUPPLIER_INFO_VALID  VARCHAR2(1) NULL ,
	SUPPLIER_INFO_MISS   VARCHAR2(1) NULL 
);

CREATE TABLE PS_ADMIN.PS_ANON_RXER_MAP_STG
(
	SEQ_ID               INTEGER NULL ,
	PHARAMCY_ID          INTEGER NULL ,
	FR_RXER_ID           INTEGER NULL ,
	FR_PSTL_CD           VARCHAR2(10) NULL ,
	FR_MINI_BRICK        VARCHAR2(20) NULL ,
	FR_PRAC_ID           INTEGER NULL ,
	TO_RXER_ID           INTEGER NULL ,
	TO_PSTL_CD           VARCHAR2(10) NULL ,
	TO_MINI_BRICK        VARCHAR2(20) NULL ,
	TO_PRAC_ID           INTEGER NULL ,
	EFF_FR_DT            DATE NULL 
);

CREATE TABLE PS_ADMIN.PS_ANON_TYP
(
	ANON_TYP_CD          VARCHAR2(10) NOT NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_ANON_TYP IS 'This entity represent type of anonimization provided for Prescriber. So far there are 3 Types available:
- Sequence 
- Bureau
- Dummy';

CREATE TABLE PS_ADMIN.PS_COHT_LOC_MAP
(
	COHT_ID              INTEGER NOT NULL ,
	COHT_TYP_CD          VARCHAR2(20) NOT NULL ,
	EFF_DT_FR            DATE NOT NULL ,
	EFF_DT_TO            DATE NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_COHT_LOC_MAP IS 'This is Mapping between Cohort and Location basing on which Prescribers are mapped to the Cohort.';

CREATE TABLE PS_ADMIN.PS_COHT_SET
(
	COHT_SET_ID          INTEGER NOT NULL ,
	COHT_SET_NM          VARCHAR2(50) NULL ,
	COHT_SET_DESC        VARCHAR2(255) NULL ,
	CTRY_ISO_CD          VARCHAR2(3) NULL ,
	SRC_FILE_ID          INTEGER NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_COHT_SET IS 'This entity represents a Set of Cohorts grouped by specific criteria (Location Hierarchy, Prescriber age, etc) for specific Client.';

CREATE TABLE PS_ADMIN.PS_COHT
(
	COHT_ID              INTEGER NOT NULL ,
	COHT_TYP_CD          VARCHAR2(20) NOT NULL ,
	COHT_SET_ID          INTEGER NULL ,
	COHT_EXTRNL_ID       VARCHAR2(255) NULL ,
	COHT_NM              VARCHAR2(50) NULL ,
	SRC_FILE_ID          INTEGER NULL ,
	ORG_ID               NUMBER(15) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_COHT IS 'This entity represent the logical group of Prescribers - Cohort, which can be grouped by Location or other specific criteria (client or non-client specific).';

CREATE TABLE PS_ADMIN.PS_COHT_TYP
(
	COHT_TYP_CD          VARCHAR2(20) NOT NULL ,
	COHT_TYP_SHORT_NM    VARCHAR2(50) NULL ,
	COHT_TYP_DESC_TXT    VARCHAR2(255) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_COHT_TYP IS 'The Cohort Type represent the origin of the Cohort definition:
- DDMS
- CPMS
- Local Office';

CREATE TABLE PS_ADMIN.PS_CTRY
(
	CTRY_ISO_CD          VARCHAR2(3) NOT NULL ,
	CTRY_NM              VARCHAR2(64) NOT NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_CTRY IS 'The COUNTRY table is a reference table which contains the list of valid geopolitical countries, as defined by the International Standards Organization (ISO), an entity of the United Nations.';

CREATE TABLE PS_ADMIN.PS_FILE
(
	SRC_FILE_ID          INTEGER NOT NULL ,
	FILE_NM              VARCHAR2(20) NULL ,
	FILE_PROC_DT         DATE NULL ,
	ORG_ID               NUMBER(15) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_FILE IS 'This entity represents Files which were sent source or target in interface between Trusted Third Party and IMS. It also containst processing info for issue tracking.';

CREATE TABLE PS_ADMIN.PS_COHT_ANON_RXER_MAP
(
	COHT_ID              INTEGER NOT NULL ,
	ANON_RXER_ID         INTEGER NOT NULL ,
	COHT_TYP_CD          VARCHAR2(20) NOT NULL ,
	EFF_DT_FR            DATE NOT NULL ,
	EFF_DT_TO            DATE NULL ,
	SRC_FILE_ID          INTEGER NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_COHT_ANON_RXER_MAP IS 'This entity represents the mapping that gathers Anonymized Prescribers into a Cohort .';

CREATE TABLE PS_ADMIN.PS_COHT_ANON_RXER_STG
(
	COHT_TYP_CD          VARCHAR2(20) NULL ,
	COHT_ID              INTEGER NULL ,
	ANON_RXER_ID         INTEGER NULL 
);

CREATE TABLE PS_ADMIN.PS_LOC
(
	LOC_ID               INTEGER NOT NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_LOC IS 'Location is a place at which activities can take place or organisations and people contacted.';

CREATE TABLE PS_ADMIN.PS_GEO_LOC
(
	LOC_ID               INTEGER NOT NULL ,
	STR_NBR              INTEGER NULL ,
	LOC_ADDR_LINE_1_TXT  VARCHAR(99) NULL ,
	LOC_ADDR_LINE_2_TXT  VARCHAR(20) NULL ,
	LOC_ADDR_LINE_3_TXT  VARCHAR(20) NULL ,
	LOC_ADDR_LINE_4_TXT  VARCHAR(20) NULL ,
	CITY_NM              VARCHAR(20) NULL ,
	PSTL_CD              VARCHAR(20) NULL ,
	POBOX_NBR            VARCHAR(20) NULL ,
	LAT_NBR              DECIMAL(11,8) NULL ,
	LON_NBR              DECIMAL(11,8) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_GEO_LOC IS 'A Geo Location is a physical address that it is possible to either visit or deliver physical items to in the case of a PO Box.
They are significant to IMS because one or more Organizations can be carrying out Healthcare related activities at this location.

A Geo Location may also have a level as part of it''s address.

note:  Postal address';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.STR_NBR IS 'If an atomocised address is required then this would store the number of the street from which access to the Geo Location is made.';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.LOC_ADDR_LINE_1_TXT IS 'This refers to the first element of an atomicised address or could contain the entire address if an atomicised address model is not used.';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.LOC_ADDR_LINE_2_TXT IS 'This refers to the second element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.LOC_ADDR_LINE_3_TXT IS 'This refers to the third element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.LOC_ADDR_LINE_4_TXT IS 'This refers to the fourth element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.CITY_NM IS 'Refers to the metrpolitan area of the Geo Location. If not located in a metropolitan area then this is not required';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.PSTL_CD IS 'This is records the Post Code for the Geo Location. Another commonly used term for this zone shortcut is ''Zip Code''.';

COMMENT ON COLUMN PS_ADMIN.PS_GEO_LOC.POBOX_NBR IS 'The Post Office Box number for the address.';

CREATE TABLE PS_ADMIN.PS_LOC_TYP
(
	LOC_TYP_CD           VARCHAR2(20) NOT NULL ,
	LOC_TYP_NM           VARCHAR2(50) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_LOC_TYP IS 'A Location Type represents type of practice that takes place in particular location.
Allowed values :
  G = GP Practice
  D = Dental Practice  
  V = Veterinary Practice
  N = Nursing / Care Home
  H = Hospital
  O = Other
 ';

CREATE TABLE PS_ADMIN.PS_LOC_GRP
(
	LOC_GRP_ID           INTEGER NOT NULL ,
	LOC_GRP_CD           VARCHAR2(20) NULL ,
	LOC_GRP_NM           VARCHAR2(100) NULL ,
	LOC_TYP_CD           VARCHAR2(20) NULL ,
	PSTL_CD              VARCHAR2(20) NULL ,
	EXTRNL_LOC_ID        VARCHAR2(20) NULL ,
	MINI_BRICK           VARCHAR2(20) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_LOC_GRP IS 'This entity represents logical grouping of physical Locations eg. Bricks which can later be assigned to Cohorts.';

CREATE TABLE PS_ADMIN.APM_LOC_APM_LOC_GRP
(
	LOC_ID               INTEGER NOT NULL ,
	LOC_GRP_ID           INTEGER NOT NULL 
);

CREATE TABLE PS_ADMIN.APM_COHT_LOC_MAP_APM_LOC_GRP
(
	COHT_ID              INTEGER NOT NULL ,
	COHT_TYP_CD          VARCHAR2(20) NOT NULL ,
	EFF_DT_FR            DATE NOT NULL ,
	LOC_GRP_ID           INTEGER NOT NULL 
);

CREATE TABLE PS_ADMIN.PS_ANON_RXER_PS_LOC_GRP
(
	ANON_RXER_ID         INTEGER NOT NULL ,
	LOC_GRP_ID           INTEGER NOT NULL 
);

CREATE TABLE PS_ADMIN.PS_ORG_ROLE_TYP
(
	ORG_ROLE_TYP_CD      VARCHAR(99) NOT NULL ,
	ORG_ROLE_TYP_SHORT_NM VARCHAR(99) NULL ,
	ORG_ROLE_TYP_NM      VARCHAR(99) NULL ,
	ORG_ROLE_TYP_DESC    VARCHAR(99) NULL ,
	EFF_FR_DT            DATE NULL ,
	EFF_TO_DT            DATE NULL ,
	ISRTED_DT            DATE NULL 
);

COMMENT ON COLUMN PS_ADMIN.PS_ORG_ROLE_TYP.EFF_FR_DT IS 'The first business date that the reference data is valid for';

COMMENT ON COLUMN PS_ADMIN.PS_ORG_ROLE_TYP.EFF_TO_DT IS 'The last business date that the reference data is valid for';

COMMENT ON COLUMN PS_ADMIN.PS_ORG_ROLE_TYP.ISRTED_DT IS 'The real time date/time that a reference data item was inserted in the GRR
';

CREATE TABLE PS_ADMIN.PS_PHARMACY
(
	SURRO_PHARMACY_ID    INTEGER NOT NULL ,
	SRC_SYS_CD           VARCHAR2(20) NULL ,
	INTR_PHARMACY_ID     VARCHAR2(20) NULL ,
	EXTRNL_PHARMACY_ID   INTEGER NULL ,
	SRC_FILE_ID          INTEGER NULL ,
	LOC_ID               INTEGER NULL ,
	ORG_ID               NUMBER(15) NULL ,
	EFF_DT_FR            DATE NULL ,
	EFF_DT_TO            DATE NULL 
);

CREATE TABLE PS_ADMIN.PS_ANON_RXER_PS_PHARMACY
(
	ANON_RXER_ID         INTEGER NOT NULL ,
	SURRO_PHARMACY_ID    INTEGER NOT NULL 
);

CREATE TABLE PS_ADMIN.PS_FILE_PROC_DETLS
(
	SRC_FILE_ID          INTEGER NOT NULL ,
	REC_CNT              INTEGER NULL ,
	DUPE_CNT             INTEGER NULL ,
	ERR_CNT              INTEGER NULL ,
	FILE_STATUS          VARCHAR2(20) NULL ,
	UPDT_CNT             INTEGER NULL ,
	SURRO_PHARMACY_ID    INTEGER NULL 
);

CREATE TABLE PS_ADMIN.PS_PHARMACY_STG
(
	PHARMACY_ID          VARCHAR2(10) NULL ,
	SYS_CD               VARCHAR2(10) NULL ,
	EXTRNL_SHOP_ID       INTEGER NULL ,
	LAT_NBR              INTEGER NULL ,
	LON_NBR              INTEGER NULL ,
	ORG_NM               VARCHAR2(100) NULL ,
	LOC_ADDR_LINE_1_TXT  VARCHAR(99) NULL ,
	LOC_ADDR_LINE_2_TXT  VARCHAR(20) NULL ,
	LOC_ADDR_LINE_3_TXT  VARCHAR(20) NULL ,
	LOC_ADDR_LINE_4_TXT  VARCHAR(20) NULL ,
	LOC_ADDR_LINE_5_TXT  VARCHAR2(200) NULL ,
	CTRY_NM              VARCHAR2(64) NOT NULL ,
	PSTL_CD              VARCHAR(20) NULL ,
	MAIN_ENT_CD          VARCHAR2(100) NULL ,
	SUB_ENT_CD           VARCHAR2(100) NULL ,
	LOC_STATUS           VARCHAR2(20) NULL ,
	PANEL_CD             VARCHAR2(20) NULL ,
	PANEL_STATUS         VARCHAR2(20) NULL ,
	PANEL_DATA_STATUS    VARCHAR2(20) NULL 
);

COMMENT ON COLUMN PS_ADMIN.PS_PHARMACY_STG.LOC_ADDR_LINE_1_TXT IS 'This refers to the first element of an atomicised address or could contain the entire address if an atomicised address model is not used.';

COMMENT ON COLUMN PS_ADMIN.PS_PHARMACY_STG.LOC_ADDR_LINE_2_TXT IS 'This refers to the second element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_PHARMACY_STG.LOC_ADDR_LINE_3_TXT IS 'This refers to the third element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_PHARMACY_STG.LOC_ADDR_LINE_4_TXT IS 'This refers to the fourth element of an atomicised address.';

COMMENT ON COLUMN PS_ADMIN.PS_PHARMACY_STG.PSTL_CD IS 'This is records the Post Code for the Geo Location. Another commonly used term for this zone shortcut is ''Zip Code''.';

CREATE TABLE PS_ADMIN.PS_PROC_PD
(
	PROC_PD_CD           VARCHAR2(20) NOT NULL ,
	PD_STRT_DT           DATE NOT NULL ,
	PD_END_DT            DATE NOT NULL 
);

CREATE TABLE PS_ADMIN.PS_PHARMACY_PROC_PD
(
	PROC_PD_CD           VARCHAR2(20) NOT NULL ,
	SURRO_PHARMACY_ID    INTEGER NOT NULL ,
	LATEST_SEQ_NBR       INTEGER NOT NULL 
);

CREATE TABLE PS_ADMIN.PS_PS_COHT_STG
(
	CLIENT_ID            INTEGER NULL ,
	COHT_SET_ID          INTEGER NULL ,
	COHT_SET_NM          VARCHAR2(50) NULL ,
	COHT_TYP_CD          VARCHAR2(20) NULL ,
	COHT_ID              INTEGER NULL ,
	COHT_NM              VARCHAR2(50) NULL ,
	LOC_ID_TYP           VARCHAR2(20) NULL ,
	RXER_ID              VARCHAR2(50) NULL ,
	LOC_ID               VARCHAR2(50) NULL 
);

CREATE TABLE PS_ADMIN.PS_RX_DUPES
(
	KEY_CLMNS_HASH       VARCHAR2(255) NULL ,
	TRANS_GUID           VARCHAR2(255) NOT NULL ,
	DETL_CLMNS_HASH      VARCHAR2(255) NOT NULL ,
	RX_DSPNSD_DT         DATE NOT NULL ,
	SUPPLIER_PHARMACY_CD VARCHAR2(20) NULL ,
	SRC_FILE_ID          INTEGER NULL 
);

CREATE TABLE PS_ADMIN.PS_ORG
(
	ORG_ID               NUMBER(15) NOT NULL ,
	CTRY_ISO_CD          VARCHAR2(3) NULL ,
	ORG_NM               VARCHAR(99) NULL ,
	EFF_FR_DT            DATE NULL ,
	EFF_TO_DT            DATE NULL ,
	ISRTED_DT            DATE NULL ,
	ORG_SHORT_NM         VARCHAR(99) NULL ,
	ORG_DESC             VARCHAR(99) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_ORG IS 'This entity represents Parties that store Prescribers data and generate anonymized data sent to IMS.';

COMMENT ON COLUMN PS_ADMIN.PS_ORG.EFF_FR_DT IS 'The first business date that the reference data is valid for';

COMMENT ON COLUMN PS_ADMIN.PS_ORG.EFF_TO_DT IS 'The last business date that the reference data is valid for';

COMMENT ON COLUMN PS_ADMIN.PS_ORG.ISRTED_DT IS 'The real time date/time that a reference data item was inserted in the GRR
';

CREATE TABLE PS_ADMIN.PS_ORG_ROLE
(
	ORG_ROLE_ID          NUMBER(15) NOT NULL ,
	ORG_ROLE_CD          VARCHAR2(20) NULL ,
	ORG_ROLE_NM          VARCHAR(99) NULL ,
	ORG_ROLE_SHORT_NM    VARCHAR(99) NULL ,
	ORG_ROLE_DESC        VARCHAR(99) NULL ,
	ORG_ROLE_TYP_CD      VARCHAR(99) NULL ,
	ORG_ID               NUMBER(15) NULL 
);

CREATE TABLE PS_ADMIN.PS_RX_HIST
(
	KEY_CLMNS_HASH       VARCHAR2(255) NOT NULL ,
	TRANS_GUID           VARCHAR2(255) NOT NULL ,
	DETL_CLMNS_HASH      VARCHAR2(255) NOT NULL ,
	RX_DSPNSD_DT         DATE NOT NULL ,
	SUPPLIER_PHARMACY_CD VARCHAR2(20) NULL 
) TABLESPACE psu_tbls_02
  PARTITION BY RANGE ("RX_DSPNSD_DT") INTERVAL (NUMTODSINTERVAL(1,'DAY')) 
  SUBPARTITION BY HASH ("SUPPLIER_PHARMACY_CD") 
  SUBPARTITIONS 5
 (PARTITION "PHARMACY_CD_PAR1"  VALUES LESS THAN (TO_DATE(' 2015-07-14 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN')) 
PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
  STORAGE(
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "PSU_TBLS_02" 
 ( SUBPARTITION "SYS_SUBP521" 
  TABLESPACE "PSU_TBLS_02" 
 NOCOMPRESS , 
  SUBPARTITION "SYS_SUBP522" 
  TABLESPACE "PSU_TBLS_02" 
 NOCOMPRESS , 
  SUBPARTITION "SYS_SUBP523" 
  TABLESPACE "PSU_TBLS_02" 
 NOCOMPRESS , 
  SUBPARTITION "SYS_SUBP524" 
  TABLESPACE "PSU_TBLS_02" 
 NOCOMPRESS , 
  SUBPARTITION "SYS_SUBP525" 
  TABLESPACE "PSU_TBLS_02" 
 NOCOMPRESS ) );

CREATE TABLE PS_ADMIN.PS_RX_STG
(
	SUPPLIER_CD          VARCHAR2(3) NULL ,
	EXTRNL_PHARMACY_ID   INTEGER NULL ,
	REC_TYP              CHAR(1) NULL ,
	RX_TYP               VARCHAR2(10) NULL ,
	RX_DSPNSD_DT         DATE NULL ,
	RX_DSPNSD_TM         INTEGER NULL ,
	RX_REPEAT_STATUS     VARCHAR2(10) NULL ,
	RXER_ID              VARCHAR2(40) NULL ,
	PAT_ID               VARCHAR2(40) NULL ,
	PAT_NATL_REF_NBR     VARCHAR2(10) NULL ,
	PAT_SEX              CHAR(1) NULL ,
	PAT_YOB              INTEGER NULL ,
	EXMT_STATUS          VARCHAR2(10) NULL ,
	RX_ID                VARCHAR2(40) NULL ,
	RX_ITEM_SEQ          INTEGER NULL ,
	SUPPLIERS_DSPNSD_DRUG_CD VARCHAR2(20) NULL ,
	DSPNSD_DRUG_IPU_CD   VARCHAR2(20) NULL ,
	DSPNSD_DRUG_DESC     VARCHAR2(150) NULL ,
	GENERIC_USE_MARKER   INTEGER NULL ,
	DSPNSD_QTY           INTEGER NULL ,
	DSPNSD_UNIT_OF_QTY   VARCHAR2(10) NULL ,
	DSPNSD_DRUG_PACK_SIZE VARCHAR2(10) NULL ,
	SUPPLIERS_PSCR_DRUG_CD VARCHAR2(20) NULL ,
	PSCR_DRUG_IPU_CD     VARCHAR2(20) NULL ,
	PSCR_DRUG_DESC       VARCHAR2(150) NULL ,
	PSCR_QTY             INTEGER NULL ,
	VERBOSE_DOSAGE       VARCHAR2(512) NULL ,
	COST_OF_DSPNSD_QTY   INTEGER NULL ,
	NRSG_HM_IND          INTEGER NULL ,
	TRANS_GUID           VARCHAR2(60) NULL ,
	KEY_CLMNS_HASH       VARCHAR2(255) NULL ,
	DETL_CLMNS_HASH      VARCHAR2(255) NULL ,
	SUPPLIER_PHARMACY_CD VARCHAR2(20) NULL ,
	PHARMACY_NM          VARCHAR2(50) NULL ,
	PHARMACY_ADDR        VARCHAR2(100) NULL 
);

CREATE TABLE PS_ADMIN.PS_RXER
(
	RXER_ID              INTEGER NOT NULL ,
	RXER_TYP_CD          VARCHAR2(10) NULL ,
	RXER_STATUS          VARCHAR2(20) NULL ,
	RXER_GRD             VARCHAR2(50) NULL ,
	RXER_SPCL_1          VARCHAR2(20) NULL ,
	RXER_SPCL_2          VARCHAR2(20) NULL ,
	RXER_SEX             VARCHAR2(5) NULL ,
	RXER_NM              VARCHAR2(50) NULL ,
	RXER_FIRST_NM        VARCHAR2(30) NULL ,
	RXER_INITS           VARCHAR2(10) NULL ,
	RXER_DT_OF_BRTH      DATE NULL ,
	RXER_QUALFCTN_YR     DATE NULL ,
	PPA_NBR              VARCHAR2(20) NULL ,
	RXER_PVT_FLG         INTEGER NULL ,
	RXER_DSPNSING_FLG    INTEGER NULL ,
	RXER_FUNDHLDER_FLG   INTEGER NULL ,
	RXER_CONSNT_FLG      INTEGER NULL ,
	RXER_GMC_NBR         INTEGER NULL ,
	RXER_PRAC_NBR        INTEGER NULL ,
	RXER_PHB_NBR         INTEGER NULL ,
	SRC_FILE_ID          INTEGER NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_RXER IS 'This entity contains data related to the Prescriber in the prescription transaction.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_STATUS IS 'Stauts of Prescriber:
- ACT, Active;
- CLO, CLosed.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_SPCL_1 IS 'Doctor�s main speciality ';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_SPCL_2 IS 'Doctor�s secondary speciality';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_SEX IS 'Prescriber Sex. 
- M = Male;
- F = Female;
- U = Unknown.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_NM IS 'Doctor�s surname';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_FIRST_NM IS 'Doctor�s first name';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_INITS IS 'Doctor�s middle initials';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_QUALFCTN_YR IS 'Doctor�s year of qualification';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.PPA_NBR IS 'PPA number if known';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_PVT_FLG IS 'Private doctor.
- Y = Yes;
- N = No.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_DSPNSING_FLG IS 'Dispensing doctor. 
- Y = Yes;
- N = No.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_FUNDHLDER_FLG IS 'Fundholding doctor. 
- Y = Yes;
- N = No.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_CONSNT_FLG IS 'Enhanced targeting consent flag.
- Y = Yes;
- N = No.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_GMC_NBR IS 'Doctor�s GMC number.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_PRAC_NBR IS 'Distinguishes different practices within same location.';

COMMENT ON COLUMN PS_ADMIN.PS_RXER.RXER_PHB_NBR IS 'Doctor�s Pharbase identifier';

CREATE TABLE PS_ADMIN.PS_RXER_PRAC_DETLS
(
	RXER_ID              INTEGER NOT NULL ,
	LOC_ID               INTEGER NOT NULL ,
	LINK_ID              INTEGER NULL ,
	LINK_STATUS          VARCHAR2(10) NULL ,
	LINK_GRD             VARCHAR2(20) NULL ,
	LOC_STAUTS           VARCHAR2(10) NULL ,
	LOC_NM               VARCHAR2(100) NULL ,
	LINK_PRTY_CD         VARCHAR2(10) NULL ,
	LOC_TYP_CD           VARCHAR2(20) NULL 
);

COMMENT ON COLUMN PS_ADMIN.PS_RXER_PRAC_DETLS.LINK_ID IS 'Unique numeric identifier for the link of doctor to practice';

COMMENT ON COLUMN PS_ADMIN.PS_RXER_PRAC_DETLS.LINK_STATUS IS 'Status of link; �ACT� = Active, �CLO� = Closed';

COMMENT ON COLUMN PS_ADMIN.PS_RXER_PRAC_DETLS.LINK_GRD IS 'Prescriber grade at this location';

COMMENT ON COLUMN PS_ADMIN.PS_RXER_PRAC_DETLS.LOC_STAUTS IS 'Status of practice; �ACT� = Active, �CLO� = Closed, + other values';

COMMENT ON COLUMN PS_ADMIN.PS_RXER_PRAC_DETLS.LOC_NM IS 'Practice Name in Location
';

COMMENT ON COLUMN PS_ADMIN.PS_RXER_PRAC_DETLS.LINK_PRTY_CD IS 'Priority of location to doctor; �1� = Primary, �2� = Secondary, � � = Unknown, + other values';

CREATE TABLE PS_ADMIN.PS_RXER_TYP
(
	RXER_TYP_CD          VARCHAR2(10) NOT NULL ,
	RXER_TYP_SHORT_NM    VARCHAR2(50) NULL ,
	RXER_TYP_DESC_TXT    VARCHAR2(255) NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_RXER_TYP IS 'This represents Type of Prescriber characterized by practice.

Allowed values :
  G = GP
  D = Dentist
  V = Vet
  N = Nurse
  H = Hospital Doc
  O = Other

';

CREATE TABLE PS_ADMIN.PS_ANON_RXER_MAP
(
	TRGT_ANON_RXER_ID    INTEGER NOT NULL ,
	ANON_RXER_ID         INTEGER NOT NULL ,
	EFF_DT_FR            DATE NOT NULL ,
	EFF_DT_TO            DATE NULL ,
	SRC_FILE_ID          INTEGER NULL 
);

COMMENT ON TABLE PS_ADMIN.PS_ANON_RXER_MAP IS 'This entity represents the mapping between several Anonymized Prescriber records that were in fact identified as belonging to one Prescriber.';

CREATE OR REPLACE procedure PS_ADMIN.IRE_CHECKTABLEEXIST_H (p_tab_name in varchar2) 

is 

tab_name varchar2(150) := p_tab_name;
n Number(3);

ext_table varchar(150) := tab_name|| '( TRANS_GUID VARCHAR2(60),OPER_FLAG CHAR(1))';


begin

select  count(*) into n from tab where TName=upper(tab_name);


if n=0 then

execute immediate 'create table ' || ext_table ;

else

execute immediate 'drop table ' || tab_name;
execute immediate 'create table ' || ext_table;

end if;
end IRE_CHECKTABLEEXIST_H;
/
CREATE OR REPLACE PROCEDURE PS_RXER_MAP_PRC (
  FILE_ID IN NUMBER
) AUTHID DEFINER IS
BEGIN
/* ------------------------- 1 ---------------------------- */

/* Inserting data into temp table to keep data only with latest from date and with fr_prac_id value */ 

insert into PS_ANON_RXER_MAP_STG_TEMP (select distinct SEQ_ID, PHARAMCY_ID, FR_RXER_ID, FR_PSTL_CD,
FR_MINI_BRICK, FR_PRAC_ID, TO_RXER_ID,
TO_PSTL_CD, TO_MINI_BRICK, TO_PRAC_ID, EFF_FR_DT from (SELECT SEQ_ID, PHARAMCY_ID, FR_RXER_ID, FR_PSTL_CD,
FR_MINI_BRICK, FR_PRAC_ID, TO_RXER_ID,
TO_PSTL_CD, TO_MINI_BRICK, TO_PRAC_ID, EFF_FR_DT, 
row_number() over (partition by SEQ_ID order by EFF_FR_DT,nvl(fr_prac_id,-1) desc) as rnk FROM ps_anon_rxer_map_stg) WHERE rnk = 1); 

/* Inserting data for SEQ_ID */

insert into PS_ANON_RXER (
ANON_RXER_ID, 
RXER_TYP_CD, 
ANON_TYP_CD,
SRC_FILE_ID,
EXTRNL_ID)
(select ANON_RXER_ID_SEQ.NEXTVAL, NULL, 'SEQUENCE', FILE_ID, SEQ_ID 
from PS_ANON_RXER_MAP_STG_TEMP 
where SEQ_ID not in (select EXTRNL_ID from PS_ANON_RXER where ANON_TYP_CD='SEQUENCE'));

/* ------------------------ 2 --------------------------- */

insert into PS_ANON_RXER (
ANON_RXER_ID, 
RXER_TYP_CD, 
ANON_TYP_CD,
SRC_FILE_ID,
EXTRNL_ID)
(select ANON_RXER_ID_SEQ.NEXTVAL, NULL, 'BUREAU', FILE_ID, M.TO_RXER_ID from (select distinct TO_RXER_ID from PS_ANON_RXER_MAP_STG) M 
where M.TO_RXER_ID not in (select EXTRNL_ID from PS_ANON_RXER where ANON_TYP_CD='BUREAU')); 

/* ------------------------ 3 --------------------------- */

/* To update the TO_DT of older seq_id's which have new bureau id */

MERGE INTO PS_ANON_RXER_MAP E
USING (select distinct D.ANON_RXER_ID, A.EFF_FR_DT-1 as EFF_DT_TO from PS_ANON_RXER_MAP_STG_TEMP A 
inner join PS_ANON_RXER B ON A.SEQ_ID=B.EXTRNL_ID 
inner join PS_ANON_RXER C on A.TO_RXER_ID=C.EXTRNL_ID 
inner join PS_ANON_RXER_MAP D ON B.ANON_RXER_ID=D.ANON_RXER_ID
where D.EFF_DT_TO IS NULL  
and D.TRGT_ANON_RXER_ID!=C.ANON_RXER_ID 
and A.EFF_FR_DT-1>D.EFF_DT_FR) F
ON (E.ANON_RXER_ID=F.ANON_RXER_ID)
WHEN MATCHED THEN
UPDATE SET E.EFF_DT_TO=F.EFF_DT_TO;

/* To insert the new value of bureau ID for older seq_id */

insert into PS_ANON_RXER_MAP (select distinct C.ANON_RXER_ID, B.ANON_RXER_ID,A.EFF_FR_DT, NULL, B.SRC_FILE_ID  from PS_ANON_RXER_MAP_STG_TEMP A 
inner join PS_ANON_RXER B on A.SEQ_ID=B.EXTRNL_ID
inner join PS_ANON_RXER C on A.TO_RXER_ID=C.EXTRNL_ID
inner join PS_ANON_RXER_MAP D on B.ANON_RXER_ID=D.ANON_RXER_ID 
where D.EFF_DT_TO IS NOT NULL 
and C.ANON_RXER_ID!=D.TRGT_ANON_RXER_ID and 
A.EFF_FR_DT>D.EFF_DT_FR and C.ANON_RXER_ID||B.ANON_RXER_ID||A.EFF_FR_DT not in (select trgt_anon_rxer_id||anon_rxer_id||eff_dt_fr from ps_anon_rxer_map));

/* To insert the new seq_id mapped with bureau id */

insert into PS_ANON_RXER_MAP (
TRGT_ANON_RXER_ID,
ANON_RXER_ID,
EFF_DT_FR,
EFF_DT_TO,
SRC_FILE_ID)
(select C.ANON_RXER_ID,B.ANON_RXER_ID, nvl(A.EFF_FR_DT, to_date('1900-01-01','yyyy-mm-dd')), NULL, B.SRC_FILE_ID from PS_ANON_RXER_MAP_STG_TEMP A
inner join PS_ANON_RXER B on A.SEQ_ID=B.EXTRNL_ID 
inner join PS_ANON_RXER C on A.TO_RXER_ID=C.EXTRNL_ID where B.ANON_RXER_ID not in (select ANON_RXER_ID from PS_ANON_RXER_MAP));

/* ------------------------ 4 --------------------------- */

/* Inserting Post code information to temp table for SEQ IDs */

INSERT INTO PS_ANON_RXER_MAP_STG_SEQ_TEMP (select SEQ_ID, FR_PRAC_ID, (case 
WHEN (((FR_PSTL_CD IS NULL) OR (upper(FR_PSTL_CD)='UNKNOWN')) 
and 
((FR_MINI_BRICK IS NULL) OR (upper(FR_MINI_BRICK) = 'UNKNOWN'))) THEN 'UNKN-UNKN'
WHEN (((FR_PSTL_CD IS NULL) OR (upper(FR_PSTL_CD)='UNKNOWN')) 
and 
((FR_MINI_BRICK IS NOT NULL) OR (upper(FR_MINI_BRICK) != 'UNKNOWN'))) THEN 'UNKN' || '-' ||FR_MINI_BRICK
WHEN (((FR_PSTL_CD IS NOT NULL) OR (upper(FR_PSTL_CD)!='UNKNOWN')) 
and 
((FR_MINI_BRICK IS NULL) OR (upper(FR_MINI_BRICK) = 'UNKNOWN'))) THEN replace(FR_PSTL_CD,' ', '') || '-' ||'UNKN'
ELSE replace(FR_PSTL_CD,' ', '') || '-' || FR_MINI_BRICK END) AS FR_PSTL_MB,
replace(nvl(upper(FR_PSTL_CD),'UNKNOWN'),' ','') as FR_PSTL_CD, nvl(upper(FR_MINI_BRICK),'UNKNOWN') as FR_MINI_BRICK, EFF_FR_DT
from PS_ANON_RXER_MAP_STG_TEMP);

/* Inserting Location Information at Practice ID level */ 

/* condition 1: prac id not null eff dt not null */ 

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PRACTICE',FR_PSTL_MB, NULL, FR_PSTL_CD,FR_PRAC_ID, FR_MINI_BRICK from (select distinct FR_PRAC_ID, FR_PSTL_MB, FR_PSTL_CD, FR_MINI_BRICK, EFF_FR_DT, 
row_number() over(partition by FR_PSTL_MB order by EFF_FR_DT desc) rnk 
from PS_ANON_RXER_MAP_STG_SEQ_TEMP where FR_PRAC_ID is not null and eff_fr_dt is not null and FR_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)) where rnk=1);

/* condition 2: prac id not null eff dt null */

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PRACTICE',FR_PSTL_MB, NULL, FR_PSTL_CD,FR_PRAC_ID, FR_MINI_BRICK from (select distinct FR_PRAC_ID, FR_PSTL_MB, FR_PSTL_CD, FR_MINI_BRICK, EFF_FR_DT, 
row_number() over(partition by FR_PSTL_MB order by EFF_FR_DT desc) rnk 
from PS_ANON_RXER_MAP_STG_SEQ_TEMP where FR_PRAC_ID is not null and eff_fr_dt is null and FR_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)) where rnk=1);

/* Inserting Location Information at Postcode-Minibrick level */

/* condition 3: prac id null eff dt null */ 

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PSTL_MB',FR_PSTL_MB, NULL, FR_PSTL_CD,FR_PRAC_ID, FR_MINI_BRICK from 
(select distinct FR_PRAC_ID, FR_PSTL_MB, FR_PSTL_CD, FR_MINI_BRICK, EFF_FR_DT
from PS_ANON_RXER_MAP_STG_SEQ_TEMP where FR_PRAC_ID is null and eff_fr_dt is null and FR_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)));

/* condition 4: prac id null eff dt not null */

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PSTL_MB',FR_PSTL_MB, NULL, FR_PSTL_CD,FR_PRAC_ID, FR_MINI_BRICK from 
(select distinct FR_PRAC_ID, FR_PSTL_MB, FR_PSTL_CD, FR_MINI_BRICK, EFF_FR_DT, row_number() over(partition by FR_PSTL_MB order by EFF_FR_DT desc) rnk
from PS_ANON_RXER_MAP_STG_SEQ_TEMP where FR_PRAC_ID is null and eff_fr_dt is not null and FR_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)) where rnk=1);

/* Inserting Post code information to temp table for BUREAU IDs */

INSERT INTO PS_ANON_RXER_MAP_STG_BUE_TEMP (select TO_RXER_ID, TO_PRAC_ID, TO_PSTL_MB, TO_PSTL_CD, TO_MINI_BRICK, EFF_FR_DT from (select TO_RXER_ID, TO_PRAC_ID, (case 
WHEN (((TO_PSTL_CD IS NULL) OR (upper(TO_PSTL_CD)='UNKNOWN')) 
and 
((TO_MINI_BRICK IS NULL) OR (upper(TO_MINI_BRICK) = 'UNKNOWN'))) THEN 'UNKN-UNKN'
WHEN (((TO_PSTL_CD IS NULL) OR (upper(TO_PSTL_CD)='UNKNOWN')) 
and 
((TO_MINI_BRICK IS NOT NULL) OR (upper(TO_MINI_BRICK) != 'UNKNOWN'))) THEN 'UNKN' || '-' ||TO_MINI_BRICK
WHEN (((TO_PSTL_CD IS NOT NULL) OR (upper(TO_PSTL_CD)!='UNKNOWN')) 
and 
((TO_MINI_BRICK IS NULL) OR (upper(TO_MINI_BRICK) = 'UNKNOWN'))) THEN replace(TO_PSTL_CD,' ', '') || '-' ||'UNKN'
ELSE replace(TO_PSTL_CD,' ', '') || '-' || TO_MINI_BRICK END) AS TO_PSTL_MB,
replace(nvl(upper(TO_PSTL_CD),'UNKNOWN'),' ','') as TO_PSTL_CD, nvl(upper(TO_MINI_BRICK),'UNKNOWN') as TO_MINI_BRICK, EFF_FR_DT, 
row_number() over (partition by TO_RXER_ID order by EFF_FR_DT desc) as rnk
from PS_ANON_RXER_MAP_STG_TEMP) where rnk=1);

/* Inserting Location for Bureau IDs at Practice ID level */

/* condition 1: prac id not null eff dt not null */

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PRACTICE',TO_PSTL_MB, NULL, TO_PSTL_CD,TO_PRAC_ID, TO_MINI_BRICK from (select distinct TO_PRAC_ID, TO_PSTL_MB, TO_PSTL_CD, TO_MINI_BRICK, EFF_FR_DT, 
row_number() over(partition by TO_PSTL_MB order by EFF_FR_DT desc) rnk 
from PS_ANON_RXER_MAP_STG_BUE_TEMP where TO_PRAC_ID is not null and eff_fr_dt is not null and TO_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)) where rnk=1);

/* condition 2: prac id not null eff dt null */

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PRACTICE',TO_PSTL_MB, NULL, TO_PSTL_CD,TO_PRAC_ID, TO_MINI_BRICK from (select distinct TO_PRAC_ID, TO_PSTL_MB, TO_PSTL_CD, TO_MINI_BRICK, EFF_FR_DT, 
row_number() over(partition by TO_PSTL_MB order by EFF_FR_DT desc) rnk 
from PS_ANON_RXER_MAP_STG_BUE_TEMP where TO_PRAC_ID is not null and eff_fr_dt is null and TO_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)) where rnk=1);

/* Inserting Location for Bureau IDs at Postcode-Minibrick level */

/* condition 3: prac id null eff dt null */ 

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PSTL_MB',TO_PSTL_MB, NULL, TO_PSTL_CD,TO_PRAC_ID, TO_MINI_BRICK from 
(select distinct TO_PRAC_ID, TO_PSTL_MB, TO_PSTL_CD, TO_MINI_BRICK, EFF_FR_DT
from PS_ANON_RXER_MAP_STG_BUE_TEMP where TO_PRAC_ID is null and eff_fr_dt is null and TO_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)));

/* condition 4: prac id null eff dt not null */

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PSTL_MB',TO_PSTL_MB, NULL, TO_PSTL_CD,TO_PRAC_ID, TO_MINI_BRICK from 
(select distinct TO_PRAC_ID, TO_PSTL_MB, TO_PSTL_CD, TO_MINI_BRICK, EFF_FR_DT, row_number() over(partition by TO_PSTL_MB order by EFF_FR_DT desc) rnk
from PS_ANON_RXER_MAP_STG_BUE_TEMP where TO_PRAC_ID is null and eff_fr_dt is not null and TO_PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)) where rnk=1); 
 
/* ------------------------ 5 --------------------------- */

/* Mapping Anon IDs with Location IDs for SEQ IDs */

MERGE INTO PS_ANON_RXER_PS_LOC_GRP  A
USING (select distinct C.ANON_RXER_ID, A.LOC_GRP_ID from PS_LOC_GRP A
inner join PS_ANON_RXER_MAP_STG_SEQ_TEMP B on A.LOC_GRP_NM=B.FR_PSTL_MB 
inner join PS_ANON_RXER C on B.SEQ_ID=C.EXTRNL_ID) B
ON (A.ANON_RXER_ID=B.ANON_RXER_ID)
WHEN MATCHED THEN 
UPDATE SET A.LOC_GRP_ID=B.LOC_GRP_ID
WHEN NOT MATCHED THEN 
INSERT (ANON_RXER_ID, LOC_GRP_ID)
VALUES (B.ANON_RXER_ID, B.LOC_GRP_ID);

/* Mapping Anon IDs with Location IDs for Bureau IDs */

MERGE INTO PS_ANON_RXER_PS_LOC_GRP  A
USING (select distinct C.ANON_RXER_ID, A.LOC_GRP_ID from PS_LOC_GRP A
inner join PS_ANON_RXER_MAP_STG_BUE_TEMP B on A.LOC_GRP_NM=B.TO_PSTL_MB  
inner join PS_ANON_RXER C on B.TO_RXER_ID=C.EXTRNL_ID) B
ON (A.ANON_RXER_ID=B.ANON_RXER_ID)
WHEN MATCHED THEN 
UPDATE SET A.LOC_GRP_ID=B.LOC_GRP_ID
WHEN NOT MATCHED THEN 
INSERT (ANON_RXER_ID, LOC_GRP_ID)
VALUES (B.ANON_RXER_ID, B.LOC_GRP_ID);

/* ------------------------ 6 --------------------------- */

/* Inserting Pharmacy Code into PS_PHARMACY */

INSERT INTO PS_PHARMACY (
SURRO_PHARMACY_ID, 
SRC_SYS_CD, 
INTR_PHARMACY_ID, 
ORG_ID)
(select SURRO_PHARMACY_ID_SEQ.NEXTVAL, 
'TTP', 
EXTRNL_SHOP_ID, 
NULL 
from (select distinct trim(leading '0' from SURRO_PHARMACY_ID) as EXTRNL_SHOP_ID from PS_ANON_RXER_MAP_STG_TEMP 
where trim(SURRO_PHARMACY_ID) not in (select INTR_PHARMACY_ID from PS_PHARMACY)));

/* Merging Pharmacy Code with ANON_ID into PS_ANON_RXER_PS_PHARMACY */

MERGE INTO PS_ANON_RXER_PS_PHARMACY A 
USING (select distinct C.ANON_RXER_ID, D.SURRO_PHARMACY_ID from PS_ANON_RXER_MAP_STG_TEMP B 
inner join PS_ANON_RXER C on B.SEQ_ID=C.EXTRNL_ID 
inner join PS_PHARMACY D on B.SURRO_PHARMACY_ID=D.INTR_PHARMACY_ID) B
ON (A.ANON_RXER_ID=B.ANON_RXER_ID) 
WHEN MATCHED THEN
UPDATE SET A.SURRO_PHARMACY_ID=B.SURRO_PHARMACY_ID
WHEN NOT MATCHED THEN
INSERT (ANON_RXER_ID, SURRO_PHARMACY_ID)
VALUES (B.ANON_RXER_ID, B.SURRO_PHARMACY_ID);

commit;

END;
/
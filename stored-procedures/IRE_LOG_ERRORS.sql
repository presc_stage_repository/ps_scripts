CREATE OR REPLACE FUNCTION IRE_LOG_ERRORS( i_buff in varchar2 )

RETURN VARCHAR2 IS
g_start_pos integer := 1;
g_end_pos  integer;
RET_VAL VARCHAR2(100);

  BEGIN
    g_end_pos := Instr ( i_buff, Chr(10), g_start_pos );

    CASE g_end_pos > 0
      WHEN true THEN
        
        RET_VAL := ( Substr ( i_buff, g_start_pos,g_end_pos-g_start_pos ) );
        g_start_pos := g_end_pos+1;
        RETURN RET_VAL;
        

      WHEN FALSE THEN
      RET_VAL := ( Substr ( i_buff, g_start_pos,(Length(i_buff)-g_start_pos)+1 ) );
        
            RETURN RET_VAL;
    END CASE;
  END IRE_LOG_ERRORS;
/
CREATE OR REPLACE procedure PS_CHECK_COHORT_TABLE (p_tab_name in varchar2) --user_tables.table_name%type)

is 

tab_name varchar2(100) := p_tab_name;

n Number(3);

--ext_table varchar(100) := tab_name|| ' as select * from PS_RX_STG WHERE 1=0';

ext_table varchar2(2000) :=  tab_name || ' (PRESCRIBER_ID varchar(40) NULL,PCO_DESCRIPTION VARCHAR2(255) NULL,COHORT_ID  VARCHAR (38) NULL) ';


begin

select  count(*) into n from tab where TName=upper(tab_name);

--dbms_output.put_line(n);

if n=0 then

execute immediate 'create table ' || ext_table ;

else

execute immediate 'drop table ' || tab_name;

execute immediate 'create table ' || ext_table;

end if;

end;
/

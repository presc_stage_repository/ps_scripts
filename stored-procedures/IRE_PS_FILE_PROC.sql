CREATE OR REPLACE PROCEDURE IRE_PS_FILE_PROC (SRC_ID          IN NUMBER,
                                          ORGNIZATION_ID      IN NUMBER,
                                          INPUT_FILE_NAME     IN VARCHAR2,
                                          UNQ_REC_OUTPUT_FILE IN VARCHAR2,
                                          REC_COUNT           IN NUMBER,
                                          DUP_COUNT           IN NUMBER,
                                          ERR_COUNT           IN NUMBER,
                                          UPD_COUNT           IN NUMBER,
                                          PHARMACY_ID         IN NUMBER 
                                          ) 
AS 
BEGIN
    MERGE INTO PS_FILE  USING DUAL on (SRC_FILE_ID=SRC_ID)
             WHEN NOT MATCHED THEN INSERT (SRC_FILE_ID,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM) VALUES (SRC_ID,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME)
             WHEN MATCHED THEN UPDATE SET FILE_NM=UNQ_REC_OUTPUT_FILE;
             
             COMMIT;
             
    MERGE INTO PS_FILE_PROC_DETLS USING DUAL on (SRC_FILE_ID=SRC_ID)
            WHEN NOT MATCHED THEN INSERT (SRC_FILE_ID,REC_CNT,DUPE_CNT,ERR_CNT,FILE_STATUS,UPDT_CNT,SURRO_PHARMACY_ID) 
            VALUES (SRC_ID,REC_COUNT,DUP_COUNT,ERR_COUNT,'STAGING',UPD_COUNT,PHARMACY_ID)         
            WHEN MATCHED THEN UPDATE SET FILE_STATUS = 'PROCESSED';
            
            COMMIT;
            
END IRE_PS_FILE_PROC;
/
CREATE OR REPLACE Function IRE_PS_GET_PROC_PD 
(
    supplier_cd          VARCHAR2,
    external_pharmacy_id VARCHAR2,
    file_proc_dt         date)

RETURN NUMBER

IS

  PRAGMA AUTONOMOUS_TRANSACTION;

  l_seq_num           NUMBER;
  l_surro_pharmacy_id NUMBER;
  l_proc_pd_cd        VARCHAR2(10);

BEGIN

  SELECT p.surro_pharmacy_id  INTO l_surro_pharmacy_id FROM ps_pharmacy p JOIN ps_org o ON p.org_id = o.org_id WHERE o.org_short_nm = supplier_cd AND 
  p.extrnl_pharmacy_id = external_pharmacy_id;
  
  SELECT proc_pd_cd INTO l_proc_pd_cd FROM ps_proc_pd WHERE TO_DATE(TO_CHAR(file_proc_dt, 'YYYYMMDD'), 'YYYYMMDD') BETWEEN PD_STRT_DT AND PD_END_DT;
  
  --SELECT proc_pd_cd INTO l_proc_pd_cd FROM ps_proc_pd WHERE to_date(file_proc_dt,'YYYYMMDD') BETWEEN PD_STRT_DT AND PD_END_DT;
  
  MERGE INTO ps_pharmacy_proc_pd p USING  (SELECT l_surro_pharmacy_id AS surro_pharmacy_id,l_proc_pd_cd AS proc_pd_cd FROM dual) d ON (p.surro_pharmacy_id = d.surro_pharmacy_id AND 
  p.proc_pd_cd = d.proc_pd_cd) WHEN NOT matched THEN INSERT (surro_pharmacy_id,proc_pd_cd,latest_seq_nbr ) VALUES (d.surro_pharmacy_id,d.proc_pd_cd,1 )
  WHEN matched THEN UPDATE SET p.latest_seq_nbr = p.latest_seq_nbr + 1 ;
  SELECT latest_seq_nbr INTO l_seq_num FROM ps_pharmacy_proc_pd WHERE surro_pharmacy_id = l_surro_pharmacy_id AND proc_pd_cd = l_proc_pd_cd;
  commit; 

RETURN l_seq_NUM;

EXCEPTION

        WHEN OTHERS THEN
        RAISE;

END;
/

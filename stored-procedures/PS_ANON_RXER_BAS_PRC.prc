CREATE OR REPLACE PROCEDURE PS_ANON_RXER_BAS_PRC (
  FILE_ID IN NUMBER
) AUTHID DEFINER IS
BEGIN

/* ------------------------- 1 ---------------------------- */

insert into PS_ANON_RXER (
ANON_RXER_ID, 
RXER_TYP_CD, 
ANON_TYP_CD,
SRC_FILE_ID,
EXTRNL_ID)
(select ANON_RXER_ID_SEQ.NEXTVAL, RXER_TYP_CD, 'SEQUENCE', FILE_ID, SEQ_ID 
from PS_ANON_RXER_BAS_STG 
where SEQ_ID not in (
select EXTRNL_ID from PS_ANON_RXER where ANON_TYP_CD='SEQUENCE'));

/* ------------------------- 2 ---------------------------- */

/* Insert the above case statement data into Temp table */

INSERT INTO PS_ANON_RXER_BAS_STG_TEMP (select SEQ_ID, (case 
WHEN (((PSTL_CD IS NULL) OR (upper(PSTL_CD)='UNKNOWN')) 
and 
((MINI_BRICK IS NULL) OR (upper(MINI_BRICK) = 'UNKNOWN'))) THEN 'UNKN-UNKN'
WHEN (((PSTL_CD IS NULL) OR (upper(PSTL_CD)='UNKNOWN')) 
and 
((MINI_BRICK IS NULL) OR (upper(MINI_BRICK) != 'UNKNOWN'))) THEN 'UNKN' || '-' ||MINI_BRICK
WHEN (((PSTL_CD IS NOT NULL) OR (upper(PSTL_CD)!='UNKNOWN')) 
and 
((MINI_BRICK IS NULL) OR (upper(MINI_BRICK) = 'UNKNOWN'))) THEN replace(PSTL_CD,' ', '') || '-' ||'UNKN'
ELSE replace(PSTL_CD,' ', '') || '-' || MINI_BRICK END) AS PSTL_MB,
replace(nvl(upper(PSTL_CD),'UNKNOWN'),' ','') as PSTL_CD,nvl(upper(MINI_BRICK),'UNKNOWN') as MINI_BRICK
from PS_ANON_RXER_BAS_STG); 

/* Inserting Location at Postal Code Level */

insert into PS_LOC_GRP (
LOC_GRP_ID,
LOC_GRP_CD,
LOC_GRP_NM,
LOC_TYP_CD,
PSTL_CD,
EXTRNL_LOC_ID,
MINI_BRICK)
(select LOC_GRP_ID_SEQ.NEXTVAL, 'PSTL_MB',PSTL_MB, NULL, PSTL_CD,NULL, MINI_BRICK from 
(select distinct PSTL_CD, MINI_BRICK, PSTL_MB from PS_ANON_RXER_BAS_STG_TEMP where PSTL_MB not in (select LOC_GRP_NM from PS_LOC_GRP)));

/* ------------------------- 3 ---------------------------- */

/* Mapping ANON IDs with Location ID */

MERGE INTO PS_ANON_RXER_PS_LOC_GRP  A
USING (select distinct C.ANON_RXER_ID, A.LOC_GRP_ID from PS_LOC_GRP A
inner join PS_ANON_RXER_BAS_STG_TEMP B on A.LOC_GRP_NM=B.PSTL_MB
inner join PS_ANON_RXER C on B.SEQ_ID=C.EXTRNL_ID ) B
ON (A.ANON_RXER_ID=B.ANON_RXER_ID)
WHEN MATCHED THEN 
UPDATE SET A.LOC_GRP_ID=B.LOC_GRP_ID
WHEN NOT MATCHED THEN 
INSERT (ANON_RXER_ID, LOC_GRP_ID)
VALUES (B.ANON_RXER_ID, B.LOC_GRP_ID);

/* ------------------------ 6 --------------------------- */

/* Inserting Pharmacy Code into PS_PHARMACY */

INSERT INTO PS_PHARMACY (
SURRO_PHARMACY_ID, 
SRC_SYS_CD, 
INTR_PHARMACY_ID, 
ORG_ID)
(select SURRO_PHARMACY_ID_SEQ.NEXTVAL, 
'TTP', 
EXTRNL_SHOP_ID, 
NULL 
from (select distinct trim(EXTRNL_SHOP_ID) as EXTRNL_SHOP_ID from PS_ANON_RXER_BAS_STG 
where trim(EXTRNL_SHOP_ID) not in (select INTR_PHARMACY_ID from PS_PHARMACY)));

/* Merging Pharmacy Code with ANON_ID into PS_ANON_RXER_PS_PHARMACY */

MERGE INTO PS_ANON_RXER_PS_PHARMACY A 
USING (select distinct B.ANON_RXER_ID, C.SURRO_PHARMACY_ID from PS_ANON_RXER_BAS_STG A 
inner join PS_ANON_RXER B on A.SEQ_ID=EXTRNL_ID 
inner join PS_PHARMACY C on to_number(A.EXTRNL_SHOP_ID)=to_number(C.INTR_PHARMACY_ID)) B
ON (A.ANON_RXER_ID=B.ANON_RXER_ID) 
WHEN MATCHED THEN
UPDATE SET A.SURRO_PHARMACY_ID=B.SURRO_PHARMACY_ID
WHEN NOT MATCHED THEN
INSERT (ANON_RXER_ID, SURRO_PHARMACY_ID)
VALUES (B.ANON_RXER_ID, B.SURRO_PHARMACY_ID);

commit;

END;
/
CREATE OR REPLACE PROCEDURE UATPS_ADMIN.IRE_DE_DUP_PROC(Dy_File_Name    IN     VARCHAR2,
                                          SUPPLIER_CD     IN      VARCHAR2,
                                          EXT_PHARMA_ID   IN OUT    VARCHAR2,
                                          FLAG_VALUE      IN     VARCHAR2,
                                          ERR_COUNT       IN     VARCHAR2,
                                          OUTPUT_STATUS   OUT    NUMBER,
                                          DUP_FILE_NAME  IN OUT    VARCHAR2,
                                          DE_DUP_OUTPUT_NAME IN VARCHAR2,
                                          DE_DUP_DUPLICATES_NAME IN VARCHAR2,
                                          Dy_File_Name_H IN VARCHAR2)
                                             
AS
   
        CLREF            SYS_REFCURSOR;    
        UNQIUE_REC_FILE  UTL_FILE.FILE_TYPE;
        DE_DUP_FILE      UTL_FILE.FILE_TYPE;
        OUT_DIR          VARCHAR2 (200) := DE_DUP_OUTPUT_NAME;
        DE_DUP_DIR   VARCHAR2 (300) := DE_DUP_DUPLICATES_NAME;
        INPUT_FILE_NAME  VARCHAR2(200) := REGEXP_REPLACE(DUP_FILE_NAME,'_duplicates_[0-9]+','');
        FLAG_VAL_N NUMBER(1);
        FLAG_VAL_I NUMBER(1);
        FLAG_VAL_U_D NUMBER(1);
        EXT_PHARMA_ID1 VARCHAR2(80);
        EXL_PHAR_ID VARCHAR2(80);
        L_REC_COUNT NUMBER :=0;
        L_DUP_COUNT NUMBER :=0;
        L_UPD_COUNT NUMBER :=0;
        L_DEL_COUNT NUMBER :=0;
        L_NEW_COUNT NUMBER :=0;
        L_IGNR_COUNT NUMBER := 0;
        SRC_ID    NUMBER (38);
        SRC_ID_D    NUMBER (38);
        SRC_ID_I    NUMBER (38);
        SEQ_NO_SHO  VARCHAR2 (38);
        ORGNIZATION_ID  NUMBER (20);
        OUTPUT_FILE   VARCHAR2 (200);
        EXT_PHARMA_LEN        NUMBER(38);
        WYYYYNNN              VARCHAR2 (100);
        DE_REC_COUNT1         VARCHAR2 (38);    
        UNIQUE_RECORDS  VARCHAR2(10000);
        DUP_RECORDS VARCHAR2(20000);
        UNQ_REC_OUTPUT_FILE   VARCHAR2 (200);   
        REC_COUNT             NUMBER (38);
        TOTL_REC_COUNT VARCHAR2(38);
        DUP_IGN_COUNT VARCHAR2(38);
        SQL1  VARCHAR2(500);
        SU_PHARMACY_ID     VARCHAR2(100);
        PHARMACY_ID1    VARCHAR2(100);   
        RNDOM_VAL       NUMBER(38);
        OUTPT_FILE      VARCHAR2(100);
        SQL_1 VARCHAR2(200);
        SQL_2 VARCHAR2(100);
        SQL_3 VARCHAR2(400);
        SQL_4 VARCHAR2(1000);
        DUPLICATE_REC_COMMENT VARCHAR2(100);
        IGNR_REC_COMMENT VARCHAR2(100);
        NO_REC_COMMENT VARCHAR2(200);
        V_ERROR  VARCHAR2(200);
        V_ERROR1  VARCHAR2(200);
        V_ERRM  VARCHAR2(200);          
        FLAG NUMBER(1);
        A1 number(38);

TYPE REC_TYP IS RECORD

   (

        TRANS_GUID         PS_RX_STG.TRANS_GUID%TYPE,
        PREV_TRANS_GUID    PS_RX_HIST.TRANS_GUID%TYPE, 
        TRN_STAT           PS_RX_STG.DEL_IND%TYPE,
        INT_PHARMACY_ID    PS_PHARMACY.INTR_PHARMACY_ID%TYPE,
        EXT_PHARMACY_ID    PS_RX_STG.EXTRNL_PHARMACY_ID%TYPE,
        PHARMACY_NAME      PS_RX_STG.PHARMACY_NM%TYPE,
        PHARMACY_ADDRESS   PS_RX_STG.PHARMACY_ADDR%TYPE,
        SP_PSCR_DRUG_CD    PS_RX_STG.SUPPLIERS_PSCR_DRUG_CD%TYPE,
        PS_DRUG_IPU_CD     PS_RX_STG.PSCR_DRUG_IPU_CD%TYPE,    
        IPU_Value          VARCHAR2(4),
        PS_DRUG_DESC       PS_RX_STG.PSCR_DRUG_DESC%TYPE,
        DS_DRUG_PACK_SIZE  PS_RX_STG.DSPNSD_DRUG_PACK_SIZE%TYPE,
        R_ID               PS_RX_STG.RX_ID%TYPE ,
        R_ITEM_SEQ         PS_RX_STG.RX_ITEM_SEQ%TYPE,
        R_REPEAT_STATUS    PS_RX_STG.RX_REPEAT_STATUS%TYPE,
        R_TYPE             PS_RX_STG.RX_TYP%TYPE,
        EX_STATUS          PS_RX_STG.EXMT_STATUS%TYPE,
        PS_QTY             PS_RX_STG.PSCR_QTY%TYPE,
        NR_HM_IND          PS_RX_STG.NRSG_HM_IND%TYPE,
        R_DSPNSD_DT        PS_RX_STG.RX_DSPNSD_DT%TYPE,
        R_DSPNSD_TM        PS_RX_STG.RX_DSPNSD_TM%TYPE,
        SP_DSPNSD_DRUG_CD  PS_RX_STG.SUPPLIERS_DSPNSD_DRUG_CD%TYPE,
        DS_DRUG_IPU_CD     PS_RX_STG.DSPNSD_DRUG_IPU_CD%TYPE,
        IPU_Value2         VARCHAR2(4),
        DS_DRUG_DESC       PS_RX_STG.DSPNSD_DRUG_DESC%TYPE, 
        GC_USE_MARKER      PS_RX_STG.GENERIC_USE_MARKER%TYPE,
        DS_UNIT_OF_QTY     PS_RX_STG.DSPNSD_UNIT_OF_QTY%TYPE, 
        DS_QTY             PS_RX_STG.DSPNSD_QTY%TYPE,
        EUR_Value1         VARCHAR2(4),    
        CT_OF_DSPNSD_QTY   PS_RX_STG.COST_OF_DSPNSD_QTY%TYPE,
        VER_DOSAGE         VARCHAR2(1024),--PS_RX_STG.VERBOSE_DOSAGE%TYPE,
        REC_STAT           VARCHAR2(9),
        RE_TYP             PS_RX_STG.REC_TYP%TYPE,
        DT_CLMNS_HASH      PS_RX_STG.DETL_CLMNS_HASH%TYPE,
        KY_CLMNS_HASH      PS_RX_STG.KEY_CLMNS_HASH%TYPE,
        DL_IND             PS_RX_STG.DEL_IND%TYPE 
   );

    REC   REC_TYP;    
    
BEGIN 
    
    SELECT ABS(DBMS_RANDOM.RANDOM) INTO RNDOM_VAL FROM DUAL;
    OUTPT_FILE := 'KF_OUTFILE_'||RNDOM_VAL||'.TXT';
     
    UNQIUE_REC_FILE := UTL_FILE.FOPEN (OUT_DIR, OUTPT_FILE, 'W');
    UTL_FILE.PUT_LINE (UNQIUE_REC_FILE, 'TransactionGUID|PrevTransactionGUID|TransactionStatusFlag|LocationDDMSIdentifier|LocationIdentifier|LocationName|LocationAddressLine1Text|PackCodeSupplier|PackCodeNational|PackCodeNationalType|PackDescription|PackSize|PrescriptionID|PrescriptionItemID|PrescriptionFillTypeCode|PrescriptonType|PrescriptionExemptionStatus|PrescribedQuantity|PrescriptionNursingHomeFlag|DispensedPrescriptionDate|DispensedPrescriptionTime|DispencedPackCodeSupplier|DispencedPackCodeNational|DispencedPackCodeNationalType|DispencedPackDescription|DispencedPackGenericFlag|DispencedPackStrengh|DispensedQuantity|DispensedValueCurrencyCode|DispensedValue|VerboseDosage');
    
    SQL_1:='SELECT EXTRNL_PHARMACY_ID  FROM '|| Dy_File_Name ||' WHERE  ROWNUM <= 1';
    
    EXECUTE IMMEDIATE SQL_1 INTO EXL_PHAR_ID;
    
    DE_DUP_FILE := UTL_FILE.FOPEN (DE_DUP_DIR, DUP_FILE_NAME, 'W');
                                  
    OPEN CLREF FOR ('SELECT
          /*+ use_hash(s,H) leading(s h) */
                
          S.TRANS_GUID AS TRANS_GUID,
          H.TRANS_GUID AS PREV_TRANS_GUID,
          CASE
            WHEN S.DEL_IND = 1 AND H.DEL_IND = 0 THEN 2
            WHEN H.TRANS_GUID IS NULL
            THEN 0
            ELSE 1
          END                             AS TRN_STAT,
          P.INTR_PHARMACY_ID              AS INT_PHARMACY_ID ,
          SUBSTR(S.EXTRNL_PHARMACY_ID,-6) AS EXT_PHARMACY_ID ,
          S.PHARMACY_NM                   AS PHARMACY_NAME ,
          S.PHARMACY_ADDR                 AS PHARMACY_ADDRESS,
          S.SUPPLIERS_PSCR_DRUG_CD        AS SP_PSCR_DRUG_CD,
          S.PSCR_DRUG_IPU_CD              AS PS_DRUG_IPU_CD,
          ''IPU''                         AS IPU_Value,
          S.PSCR_DRUG_DESC                AS PS_DRUG_DESC,
          S.DSPNSD_DRUG_PACK_SIZE         AS DS_DRUG_PACK_SIZE,
          S.RX_ID                         AS R_ID,
          S.RX_ITEM_SEQ                   AS R_ITEM_SEQ,
          S.RX_REPEAT_STATUS              AS R_REPEAT_STATUS,
          S.RX_TYP                        AS R_TYPE,
          S.EXMT_STATUS                   AS EX_STATUS,
          S.PSCR_QTY                      AS PS_QTY,
          S.NRSG_HM_IND                   AS NR_HM_IND,
          S.RX_DSPNSD_DT                  AS R_DSPNSD_DT,
          S.RX_DSPNSD_TM                  AS R_DSPNSD_TM,
          S.SUPPLIERS_DSPNSD_DRUG_CD      AS SP_DSPNSD_DRUG_CD,
          S.DSPNSD_DRUG_IPU_CD            AS DS_DRUG_IPU_CD,
          ''IPU''                         AS IPU_Value2,
          S.DSPNSD_DRUG_DESC              AS DS_DRUG_DESC,
          S.GENERIC_USE_MARKER            AS GC_USE_MARKER,
          S.DSPNSD_UNIT_OF_QTY            AS DS_UNIT_OF_QTY,
          S.DSPNSD_QTY                    AS DS_QTY,
          ''EUR''                         AS EUR_Value1,
          S.COST_OF_DSPNSD_QTY            AS CT_OF_DSPNSD_QTY ,
          S.VERBOSE_DOSAGE                AS VER_DOSAGE,
          CASE WHEN S.DEL_IND = 1 AND H.DEL_IND = 0 THEN ''DEL''
               WHEN S.DEL_IND = 1 AND H.DEL_IND IS NULL THEN ''DEL_IGNR''
                                 WHEN H.DEL_IND = 1 THEN ''IGNR''
                                 WHEN S2.TRANS_GUID IS NOT NULL THEN ''IGNR''
               WHEN S.DETL_CLMNS_HASH <> H.DETL_CLMNS_HASH THEN ''UPDT''
               WHEN S.DETL_CLMNS_HASH = H.DETL_CLMNS_HASH THEN ''DUPL''
               WHEN H.DETL_CLMNS_HASH IS NULL THEN ''NEW''
               ELSE ''ERR'' END AS REC_STAT,
          S.REC_TYP                       AS RE_TYP,     
          S.DETL_CLMNS_HASH               AS DT_CLMNS_HASH,
          S.KEY_CLMNS_HASH                AS KY_CLMNS_HASH,     
          S.DEL_IND                       AS DL_IND
          
        FROM
          (SELECT stg.*,
            row_number() over ( partition BY key_clmns_hash, rx_dspnsd_dt, del_ind ORDER BY 1) AS RN
          FROM  '   || Dy_File_Name || '   stg
          ) s
        LEFT JOIN ps_pharmacy p ON s.extrnl_pharmacy_id = p.extrnl_pharmacy_id  
        LEFT JOIN ps_rx_hist H
        ON h.key_clmns_hash        = s.key_clmnS_hash
        AND h.rx_dspnsd_dt         = s.rx_dspnsd_dt
        AND s.supplier_pharmacy_cd = h.SUPPLIER_PHARMACY_CD
                    LEFT JOIN (SELECT stg.*,
            row_number() over ( partition BY key_clmns_hash, rx_dspnsd_dt, del_ind ORDER BY 1) AS RN
          FROM  '   || Dy_File_Name || '   stg
          ) s2 ON s2.key_clmns_hash        = s.key_clmnS_hash
        AND s2.rx_dspnsd_dt         = s.rx_dspnsd_dt
        AND s.supplier_pharmacy_cd = s2.SUPPLIER_PHARMACY_CD
        AND s.del_ind <> s2.del_ind
                    AND S2.RN = 1
        WHERE s.RN                 = 1'); 
      
    LOOP
    
      FETCH CLREF  INTO REC;
      EXIT WHEN CLREF%NOTFOUND;  --CLREF%COUNT
         
            IF REC.REC_STAT = 'NEW'
            THEN
                
                --dbms_output.put_line(REC.INT_PHARMACY_ID);
                L_NEW_COUNT := L_NEW_COUNT + 1;
                
                SQL1:='INSERT INTO '|| Dy_File_Name_H ||'(TRANS_GUID,OPER_FLAG) VALUES( '''|| REC.TRANS_GUID ||''' , ''N'')';
                EXECUTE IMMEDIATE SQL1;
                           
                UNIQUE_RECORDS:='"'|| REC.TRANS_GUID || '"|"'||REC.PREV_TRANS_GUID || '"|' || REC.TRN_STAT || '|'  || REC.INT_PHARMACY_ID || '|' || REC.EXT_PHARMACY_ID || '|"' ||
                   REC.PHARMACY_NAME || '"|"' || REC.PHARMACY_ADDRESS || '"|' || REC.SP_PSCR_DRUG_CD || '|' || REC.PS_DRUG_IPU_CD || '|"' || REC.IPU_Value || '"|"' || REC.PS_DRUG_DESC || 
                   '"|' || REC.DS_DRUG_PACK_SIZE || '|"' || REC.R_ID || '"|"' || REC.R_ITEM_SEQ || '"|' || REC.R_REPEAT_STATUS || '|"'|| REC.R_TYPE || '"|"'    
                   || REC.EX_STATUS || '"|' || REC.PS_QTY || '|' || REC.NR_HM_IND || '|'|| to_char(to_date(REC.R_DSPNSD_DT,'dd-mon-yy' ),'yyyymmdd') || '|'|| REC.R_DSPNSD_TM || '|' 
                   || REC.SP_DSPNSD_DRUG_CD || '|' || REC.DS_DRUG_IPU_CD || '|"' || REC.IPU_Value2 || '"|"' || REC.DS_DRUG_DESC  || '"|' || REC.GC_USE_MARKER || '|"' 
                   || REC.DS_UNIT_OF_QTY || '"|' || REC.DS_QTY || '|"' || REC.EUR_Value1 || '"|' || REC.CT_OF_DSPNSD_QTY || '|"' || replace(REC.VER_DOSAGE,'"','""') || '"'; 
                
                UTL_FILE.PUT_LINE (UNQIUE_REC_FILE,UNIQUE_RECORDS);   
                
                FLAG_VAL_N:=0;
                    
            ELSIF REC.REC_STAT = 'UPDT'   
            THEN
                  
                L_UPD_COUNT := L_UPD_COUNT + 1;
                
                UNIQUE_RECORDS:='"'|| REC.TRANS_GUID || '"|"'||REC.PREV_TRANS_GUID || '"|' || REC.TRN_STAT || '|'  || REC.INT_PHARMACY_ID || '|' || REC.EXT_PHARMACY_ID || '|"' ||
                   REC.PHARMACY_NAME || '"|"' || REC.PHARMACY_ADDRESS || '"|' || REC.SP_PSCR_DRUG_CD || '|' || REC.PS_DRUG_IPU_CD || '|"' || REC.IPU_Value || '"|"' || REC.PS_DRUG_DESC || 
                   '"|' || REC.DS_DRUG_PACK_SIZE || '|"' || REC.R_ID || '"|"' || REC.R_ITEM_SEQ || '"|' || REC.R_REPEAT_STATUS || '|"'|| REC.R_TYPE || '"|"'    
                   || REC.EX_STATUS || '"|' || REC.PS_QTY || '|' || REC.NR_HM_IND || '|'|| to_char(to_date(REC.R_DSPNSD_DT,'dd-mon-yy' ),'yyyymmdd') || '|'|| REC.R_DSPNSD_TM || '|' 
                   || REC.SP_DSPNSD_DRUG_CD || '|' || REC.DS_DRUG_IPU_CD || '|"' || REC.IPU_Value2 || '"|"' || REC.DS_DRUG_DESC  || '"|' || REC.GC_USE_MARKER || '|"' 
                   || REC.DS_UNIT_OF_QTY || '"|' || REC.DS_QTY || '|"' || REC.EUR_Value1 || '"|' || REC.CT_OF_DSPNSD_QTY || '|"' || replace(REC.VER_DOSAGE,'"','""') || '"'; 
                
                UTL_FILE.PUT_LINE (UNQIUE_REC_FILE,UNIQUE_RECORDS);
                
                SQL1:='INSERT INTO '|| Dy_File_Name_H ||'(TRANS_GUID,OPER_FLAG) VALUES( '''|| REC.TRANS_GUID ||''' , ''U'')';
                EXECUTE IMMEDIATE SQL1;
                
                FLAG_VAL_U_D:=1;
                
            ELSIF REC.REC_STAT = 'DUPL'  -- DUPLICATE
            THEN
                           
                L_DUP_COUNT := L_DUP_COUNT + 1; -- INCREASE THE DUPLICATES COUNT RECORDS COUNT
       
               DUP_RECORDS := 
                '"' || trim(REC.RE_TYP) || '","' || REC.R_TYPE || '","' || to_char(to_date(REC.R_DSPNSD_DT,'dd-mon-yy' ),'yyyymmdd') || '","' || REC.R_DSPNSD_TM || '","' || 
                REC.R_REPEAT_STATUS || '","' || REC.EX_STATUS || '","' || REC.R_ID || '","' || REC.R_ITEM_SEQ || '","' || REC.SP_DSPNSD_DRUG_CD || '","' || REC.DS_DRUG_IPU_CD || '","' 
                || REC.DS_DRUG_DESC || '","' || REC.GC_USE_MARKER || '","' || REC.DS_UNIT_OF_QTY || '","' || REC.DS_UNIT_OF_QTY || '","' || REC.DS_DRUG_PACK_SIZE || '","' || 
                REC.SP_PSCR_DRUG_CD || '","' || REC.PS_DRUG_IPU_CD || '","' || REC.PS_DRUG_DESC || '","' || REC.PS_QTY || '","'  || REC.CT_OF_DSPNSD_QTY || '","' || REC.NR_HM_IND || '","' || REC.DL_IND || '","' || REC.TRANS_GUID || '"'; 
                
                UTL_FILE.PUT_LINE (DE_DUP_FILE, DUP_RECORDS);
                
                SRC_ID_D := SRC_FILE_ID_SEQ.NEXTVAL;
        
                --INSERT INTO PS_FILE (SRC_FILE_ID,INPUT_FILE_NM,FILE_PROC_DT,ORG_ID,FILE_STATUS) VALUES (SRC_ID,DUP_FILE_NAME,SYSDATE,ORGNIZATION_ID,'SUCCESS');
                INSERT INTO PS_FILE (SRC_FILE_ID,INPUT_FILE_NM,FILE_PROC_DT,ORG_ID,FILE_STATUS) VALUES (SRC_ID_D,DUP_FILE_NAME,SYSDATE,ORGNIZATION_ID,'DUPLICATE');
                INSERT INTO PS_RX_DUPES(TRANS_GUID,DETL_CLMNS_HASH,KEY_CLMNS_HASH,RX_DSPNSD_DT,SUPPLIER_PHARMACY_CD,SRC_FILE_ID)   -- INSERT INTO DUPES TABLE 
                VALUES (REC.TRANS_GUID,REC.KY_CLMNS_HASH,REC.KY_CLMNS_HASH,REC.R_DSPNSD_DT,EXT_PHARMA_ID,SRC_ID_D);
                
                SQL1:='INSERT INTO '|| Dy_File_Name_H ||'(TRANS_GUID,OPER_FLAG) VALUES( '''|| REC.TRANS_GUID ||''' , ''D'')';
                EXECUTE IMMEDIATE SQL1;
                
            ELSIF REC.REC_STAT = 'DEL' THEN-- DELETION
        
                L_DEL_COUNT := L_DEL_COUNT +1;
                                    
                UNIQUE_RECORDS:='"'|| REC.TRANS_GUID || '"|"'||REC.PREV_TRANS_GUID || '"|' || REC.TRN_STAT || '|'  || REC.INT_PHARMACY_ID || '|' || REC.EXT_PHARMACY_ID || '|"' ||
                REC.PHARMACY_NAME || '"|"' || REC.PHARMACY_ADDRESS || '"|' || REC.SP_PSCR_DRUG_CD || '|' || REC.PS_DRUG_IPU_CD || '|"' || REC.IPU_Value || '"|"' || REC.PS_DRUG_DESC || 
                '"|' || REC.DS_DRUG_PACK_SIZE || '|"' || REC.R_ID || '"|"' || REC.R_ITEM_SEQ || '"|' || REC.R_REPEAT_STATUS || '|"'|| REC.R_TYPE || '"|"'    
                || REC.EX_STATUS || '"|' || REC.PS_QTY || '|' || REC.NR_HM_IND || '|'|| to_char(to_date(REC.R_DSPNSD_DT,'dd-mon-yy' ),'yyyymmdd')    
                || '|'|| REC.R_DSPNSD_TM || '|' || REC.SP_DSPNSD_DRUG_CD || '|' || REC.DS_DRUG_IPU_CD || '|"' || REC.IPU_Value2 || '"|"'    
                || REC.DS_DRUG_DESC  || '"|' || REC.GC_USE_MARKER || '|"' || REC.DS_UNIT_OF_QTY || '"|' || REC.DS_QTY || '|"' || REC.EUR_Value1    
                || '"|' || REC.CT_OF_DSPNSD_QTY || '|"' || replace(REC.VER_DOSAGE,'"','""') || '"';
                
                UTL_FILE.PUT_LINE (UNQIUE_REC_FILE,UNIQUE_RECORDS);
    
                SQL1:='INSERT INTO '|| Dy_File_Name_H ||'(TRANS_GUID,OPER_FLAG) VALUES( '''|| REC.TRANS_GUID ||''' , ''X'')';
                EXECUTE IMMEDIATE SQL1;
                
                FLAG_VAL_U_D:=1;
                    
            ELSIF REC.REC_STAT = 'DEL_IGNR' OR REC.REC_STAT = 'IGNR' THEN  
                
                L_IGNR_COUNT := L_IGNR_COUNT +1;   
                
                SRC_ID_I := SRC_FILE_ID_SEQ.NEXTVAL;
                SELECT ORG_ID INTO ORGNIZATION_ID FROM PS_ORG WHERE ORG_SHORT_NM = SUPPLIER_CD;
                SQL_2:='SELECT SUPPLIER_PHARMACY_CD  FROM '|| Dy_File_Name ||' WHERE ROWNUM=1';
                EXECUTE IMMEDIATE SQL_2 INTO PHARMACY_ID1;

                --INSERT INTO PS_FILE (SRC_FILE_ID,INPUT_FILE_NM,FILE_PROC_DT,ORG_ID,FILE_STATUS) VALUES (SRC_ID,INPUT_FILE_NAME,SYSDATE,ORGNIZATION_ID,'SUCCESS'); 
                INSERT INTO PS_FILE (SRC_FILE_ID,INPUT_FILE_NM,FILE_PROC_DT,ORG_ID,FILE_STATUS) VALUES (SRC_ID_I,INPUT_FILE_NAME,SYSDATE,ORGNIZATION_ID,'IGNORE');
    
                INSERT INTO PS_RX_IGNR(TRANS_GUID,DETL_CLMNS_HASH,KEY_CLMNS_HASH,RX_DSPNSD_DT,SUPPLIER_PHARMACY_CD,SRC_FILE_ID) 
                VALUES(REC.TRANS_GUID,REC.DT_CLMNS_HASH,REC.KY_CLMNS_HASH,REC.R_DSPNSD_DT,PHARMACY_ID1,SRC_ID_I);
                
                SQL1:='INSERT INTO '|| Dy_File_Name_H ||'(TRANS_GUID,OPER_FLAG) VALUES( '''|| REC.TRANS_GUID ||''' , ''I'')';
                EXECUTE IMMEDIATE SQL1;
                
                --FLAG_VAL_I:=2;  
            
            END IF; 
                
    END LOOP;
    TOTL_REC_COUNT:=CLREF%ROWCOUNT;
    CLOSE CLREF;
    
    
    SELECT PROC_PD_CD INTO WYYYYNNN FROM PS_PROC_PD WHERE PD_STRT_DT <= TRUNC (SYSDATE - 7) AND PD_END_DT >= TRUNC (SYSDATE - 7);
    
    SEQ_NO_SHO := IRE_PS_GET_PROC_PD(SUPPLIER_CD, EXT_PHARMA_ID, sysdate - 7);
    SEQ_NO_SHO := LPAD (SEQ_NO_SHO, 4, '0');
    DE_REC_COUNT1 := LPAD(DE_REC_COUNT1, 7, '0');
     
    SELECT REGEXP_REPLACE(EXT_PHARMA_ID,'[A-Z,a-z,_]') INTO EXT_PHARMA_ID1  FROM DUAL;
            
    EXT_PHARMA_LEN:= LENGTH(EXT_PHARMA_ID1); 
    IF EXT_PHARMA_LEN = 5
    THEN
      
        EXT_PHARMA_ID := LPAD(EXT_PHARMA_ID1, 6, '0');
      
    END IF;
    
    SELECT ORG_ID INTO ORGNIZATION_ID FROM PS_ORG WHERE ORG_SHORT_NM = SUPPLIER_CD;
        
    L_NEW_COUNT := L_NEW_COUNT + L_UPD_COUNT + L_DEL_COUNT;
    
    DUP_IGN_COUNT := L_DUP_COUNT + L_IGNR_COUNT;  
         
    UNQ_REC_OUTPUT_FILE := 'LRXIE' || FLAG_VALUE || '10_' || SUPPLIER_CD || '_' || EXT_PHARMA_ID1 || '_' || WYYYYNNN || '_' || SEQ_NO_SHO || '_' || L_NEW_COUNT || '.TXT';   
    
    IF L_IGNR_COUNT = TOTL_REC_COUNT THEN    --if all records are ignore
    
        IF  L_NEW_COUNT = 0 THEN
            IF DUP_IGN_COUNT =  L_IGNR_COUNT THEN
                    
     --               SRC_ID := SRC_FILE_ID_SEQ.NEXTVAL;
        
                    SQL_3:='SELECT SURRO_PHARMACY_ID FROM PS_PHARMACY WHERE EXTRNL_PHARMACY_ID = ( SELECT EXTRNL_PHARMACY_ID  FROM '|| Dy_File_Name ||' WHERE ROWNUM= 1)';
                    EXECUTE IMMEDIATE SQL_3 INTO SU_PHARMACY_ID;
        
                    --INSERT INTO PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) VALUES (SRC_ID,UNQ_REC_OUTPUT_FILE,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME,'SUCCESS');
       --             INSERT INTO PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) VALUES (SRC_ID,UNQ_REC_OUTPUT_FILE,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME,'NEW');
        
                    INSERT INTO PS_FILE_PROC_DETLS(SRC_FILE_ID,REC_CNT,DUPE_CNT,ERR_CNT,FILE_STATUS,UPDT_CNT,IGNR_CNT,DEL_CNT,SURRO_PHARMACY_ID)       
                    VALUES(SRC_ID_I,TOTL_REC_COUNT,L_DUP_COUNT,ERR_COUNT,'IGNORE',L_UPD_COUNT,L_IGNR_COUNT,L_DEL_COUNT,SU_PHARMACY_ID);
        
                    OUTPUT_STATUS := 2;
                    IGNR_REC_COMMENT := 'There is no unique record generated ';
                    DBMS_OUTPUT.PUT_LINE ('"'|| OUTPUT_STATUS || '|' || IGNR_REC_COMMENT ||  '|' || OUTPT_FILE || '"');
                     
            END IF;
        END IF;
    
    END IF;
    
    IF TOTL_REC_COUNT = L_DUP_COUNT THEN    --if all records are duplicate
    
           IF  L_NEW_COUNT = 0 THEN
                IF DUP_IGN_COUNT =  L_DUP_COUNT THEN                   
                    SRC_ID := SRC_FILE_ID_SEQ.NEXTVAL;
        
                    SQL_3:='SELECT SURRO_PHARMACY_ID FROM PS_PHARMACY WHERE EXTRNL_PHARMACY_ID = ( SELECT EXTRNL_PHARMACY_ID  FROM '|| Dy_File_Name ||' WHERE ROWNUM= 1)';
                    EXECUTE IMMEDIATE SQL_3 INTO SU_PHARMACY_ID;
        
                    --INSERT INTO PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) VALUES (SRC_ID,UNQ_REC_OUTPUT_FILE,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME,'SUCCESS');
                   -- INSERT INTO PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) 
                    --VALUES (SRC_ID,UNQ_REC_OUTPUT_FILE,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME,'NEW');
                    
                    INSERT INTO PS_FILE_PROC_DETLS(SRC_FILE_ID,REC_CNT,DUPE_CNT,ERR_CNT,FILE_STATUS,UPDT_CNT,IGNR_CNT,DEL_CNT,SURRO_PHARMACY_ID)       
                    VALUES(SRC_ID_D,TOTL_REC_COUNT,L_DUP_COUNT,ERR_COUNT,'DUPLICATE',L_UPD_COUNT,L_IGNR_COUNT,L_DEL_COUNT,SU_PHARMACY_ID);
                    
                    OUTPUT_STATUS := 1;
                    DUPLICATE_REC_COMMENT := 'Complete file contains duplicate records';
                    DBMS_OUTPUT.PUT_LINE ('"'|| OUTPUT_STATUS || '|' || DUPLICATE_REC_COMMENT ||  '|' || OUTPT_FILE || '"');
                END IF;
           END IF;     
    END IF;
    
    IF DUP_IGN_COUNT = TOTL_REC_COUNT THEN   --if all records are duplicate and ignore
    
        IF L_NEW_COUNT = 0 THEN
            
              IF L_DUP_COUNT !=  0 and L_IGNR_COUNT != 0 THEN      
                    
                    SQL_3:='SELECT SURRO_PHARMACY_ID FROM PS_PHARMACY WHERE EXTRNL_PHARMACY_ID = ( SELECT EXTRNL_PHARMACY_ID  FROM '|| Dy_File_Name ||' WHERE ROWNUM= 1)';
                    EXECUTE IMMEDIATE SQL_3 INTO SU_PHARMACY_ID;
        
                    INSERT INTO PS_FILE_PROC_DETLS(SRC_FILE_ID,REC_CNT,DUPE_CNT,ERR_CNT,FILE_STATUS,UPDT_CNT,IGNR_CNT,DEL_CNT,SURRO_PHARMACY_ID)       
                    VALUES(SRC_ID_D,TOTL_REC_COUNT,L_DUP_COUNT,ERR_COUNT,'DUPLICATE',L_UPD_COUNT,L_IGNR_COUNT,L_DEL_COUNT,SU_PHARMACY_ID);
                    
                    OUTPUT_STATUS := 5;
                    IGNR_REC_COMMENT := 'All records are ignore/duplicate due to this input file moves into duplicate folder';
                    DBMS_OUTPUT.PUT_LINE ('"'|| OUTPUT_STATUS || '|' || IGNR_REC_COMMENT ||  '|' || OUTPT_FILE || '"'); 
              END IF;      
        END IF;
    
    END IF;
    
    IF FLAG_VAL_N = 0 THEN       -- if records are unique
                  

        SRC_ID := SRC_FILE_ID_SEQ.NEXTVAL;
        
        SQL_3:='SELECT SURRO_PHARMACY_ID FROM PS_PHARMACY WHERE EXTRNL_PHARMACY_ID = ( SELECT EXTRNL_PHARMACY_ID  FROM '|| Dy_File_Name ||' WHERE ROWNUM= 1)';
        EXECUTE IMMEDIATE SQL_3 INTO SU_PHARMACY_ID;
  
        INSERT INTO PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) VALUES (SRC_ID,UNQ_REC_OUTPUT_FILE,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME,'NEW');      
        
        INSERT INTO PS_FILE_PROC_DETLS(SRC_FILE_ID,REC_CNT,DUPE_CNT,ERR_CNT,FILE_STATUS,UPDT_CNT,IGNR_CNT,DEL_CNT,SURRO_PHARMACY_ID)       
        VALUES(SRC_ID,TOTL_REC_COUNT,L_DUP_COUNT,ERR_COUNT,'NEW',L_UPD_COUNT,L_IGNR_COUNT,L_DEL_COUNT,SU_PHARMACY_ID);
        
        SQL_4 := 'INSERT INTO PS_FILE_DT_PROC_DETLS (SURRO_PHARMACY_ID, SRC_FILE_ID, RX_DSPNSD_DT, DUPE_CNT, REC_CNT,UPDT_CNT, DEL_CNT, IGNR_CNT) ' ||
        ' SELECT /*+ USE_HASH(S,H) */ '||SU_PHARMACY_ID||','|| SRC_ID||', ' || 
        '  S.RX_DSPNSD_DT, SUM(CASE WHEN H.OPER_FLAG IS NULL OR H.OPER_FLAG = ''D'' THEN 1 ELSE 0 END) AS DUPE_CNT, ' ||
        ' COUNT(*) AS REC_CNT, SUM(CASE WHEN H.OPER_FLAG = ''U'' THEN 1 ELSE 0 END) AS UPDT_CNT,  SUM(CASE WHEN H.OPER_FLAG = ''X'' THEN 1 ELSE 0 END) AS DEL_CNT '||
        ' ,  SUM(CASE WHEN H.OPER_FLAG = ''I'' OR H.OPER_FLAG IS NULL THEN 1 ELSE 0 END) AS IGNR_CNT ' ||
        ' FROM '|| Dy_File_Name ||' S '||
        ' LEFT JOIN '|| Dy_File_Name ||'_H H '||
        ' ON S.TRANS_GUID = H.TRANS_GUID '||
        ' GROUP BY S.RX_DSPNSD_DT';
      
        EXECUTE IMMEDIATE SQL_4;    

        OUTPUT_STATUS := 0;
        FLAG :=0;
        
    END IF;
    
 
    IF FLAG_VAL_U_D = 1 THEN  --if records are updated and deleted 
         
         SRC_ID := SRC_FILE_ID_SEQ.NEXTVAL;
        
         SQL_3:='SELECT SURRO_PHARMACY_ID FROM PS_PHARMACY WHERE EXTRNL_PHARMACY_ID = ( SELECT EXTRNL_PHARMACY_ID  FROM '|| Dy_File_Name ||' WHERE ROWNUM= 1)';
        
         EXECUTE IMMEDIATE SQL_3 INTO SU_PHARMACY_ID;
        
         INSERT INTO PS_FILE (SRC_FILE_ID,FILE_NM,FILE_PROC_DT,ORG_ID,INPUT_FILE_NM,FILE_STATUS) VALUES (SRC_ID,UNQ_REC_OUTPUT_FILE,SYSDATE,ORGNIZATION_ID,INPUT_FILE_NAME,'NEW');
        
         INSERT INTO PS_FILE_PROC_DETLS(SRC_FILE_ID,REC_CNT,DUPE_CNT,ERR_CNT,FILE_STATUS,UPDT_CNT,IGNR_CNT,DEL_CNT,SURRO_PHARMACY_ID)       
         VALUES(SRC_ID,TOTL_REC_COUNT,L_DUP_COUNT,ERR_COUNT,'NEW',L_UPD_COUNT,L_IGNR_COUNT,L_DEL_COUNT,SU_PHARMACY_ID);   
        
         UNQ_REC_OUTPUT_FILE := 'LRXIE' || FLAG_VALUE || '10_' || SUPPLIER_CD || '_' || EXT_PHARMA_ID1 || '_' || WYYYYNNN || '_' || SEQ_NO_SHO || '_' || L_NEW_COUNT || '.TXT';
         OUTPUT_STATUS := 0;
         FLAG :=0;
                 
    END IF;
    
    IF FLAG = 0 THEN
    
        DBMS_OUTPUT.PUT_LINE ('"'|| OUTPUT_STATUS || '|' || UNQ_REC_OUTPUT_FILE ||  '|' || OUTPT_FILE || '"');
        
    END IF;
        
    COMMIT; 
    UTL_FILE.FCLOSE (UNQIUE_REC_FILE);
    UTL_FILE.FCLOSE (DE_DUP_FILE);
    
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        OUTPUT_STATUS := 3;
        NO_REC_COMMENT := ' There is no records to process in dynamic table ';
        DBMS_OUTPUT.PUT_LINE ('"'|| OUTPUT_STATUS || '|' || NO_REC_COMMENT ||  '|' || OUTPT_FILE || '"');
    WHEN UTL_FILE.INVALID_FILEHANDLE THEN
        UTL_FILE.FCLOSE_ALL;
    WHEN UTL_FILE.INVALID_PATH THEN
        UTL_FILE.FCLOSE_ALL;
    WHEN UTL_FILE.READ_ERROR THEN
        UTL_FILE.FCLOSE_ALL;
    WHEN UTL_FILE.WRITE_ERROR THEN
        UTL_FILE.FCLOSE_ALL;
    WHEN OTHERS  THEN
        UTL_FILE.FCLOSE_ALL;
        OUTPUT_STATUS := 4;

   DBMS_OUTPUT.PUT_LINE('"' || OUTPUT_STATUS || '| The error is '|| IRE_LOG_ERRORS(DBMS_UTILITY.FORMAT_ERROR_STACK()) ||', '|| IRE_LOG_ERRORS(DBMS_UTILITY.FORMAT_ERROR_BACKTRACE()) || '|' || OUTPT_FILE || '"');
        
END;
/
CREATE OR REPLACE procedure PS_ADMIN.PS_COHT_GEN_PRC (
  FILE_NAME IN VARCHAR2
) AUTHID DEFINER IS

v_sql varchar2(2000);

BEGIN

v_sql := ' insert into PS_COHT_GEN (select COHT_ID_SEQ.nextval,COHORT_ID 
from (select distinct COHORT_ID 
from ' ||  FILE_NAME || '
where COHORT_ID not in (select EXTRNL_BRK_ID from PS_COHT_GEN))) ';

Execute immediate v_sql;

commit;

END;
/
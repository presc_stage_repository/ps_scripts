CREATE OR REPLACE PROCEDURE CSET_FILE_VALIDATION (FILENAME IN varchar2)  
IS
c1 varchar(50);
c2 number;

--    CURSOR c_cset is 
--    select cohort_id, count(distinct(prescriber_id)) as cohort_count from FILENAME group by cohort_id having count(distinct(prescriber_id))<50;
--    CURSOR cset_null is    
--        select cohort_id, prescriber_id from FILENAME where cohort_id is null;

c_cset sys_refcursor;
cset_null sys_refcursor;

BEGIN
OPEN c_cset for 'select cohort_id, count(distinct(prescriber_id)) as cohort_count from ' || FILENAME || ' group by cohort_id having count(distinct(prescriber_id))<50';
LOOP
FETCH c_cset INTO c1,c2;
EXIT WHEN c_cset%NOTFOUND;
IF c2<50
THEN
    dbms_output.put_line('FAILED: Cohort ID ' || c1 || ' have ' ||c2||' prescribers');
END IF;
END LOOP;
CLOSE c_cset;
OPEN cset_null for 'select cohort_id, prescriber_id from '|| FILENAME || ' where cohort_id is null';
FETCH cset_null INTO c1,c2;
IF cset_null%FOUND
THEN
    dbms_output.put_line('FAILED: Null Value exist in Cohort ID FAILED');
END IF;
CLOSE cset_null;
END;
/
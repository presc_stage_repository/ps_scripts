create or replace procedure IRE_PS_LOAD_RX_HIST(tbl_nm in varchar2)
as 
begin

EXECUTE IMMEDIATE 'DELETE /*+ USE_HASH(H) */ FROM 
	 PS_RX_HIST H 
	 WHERE EXISTS 
	 (SELECT 1
		FROM ' || TBL_NM || ' S  
		JOIN ' || TBL_NM || '_H H ON S.TRANS_GUID = H.TRANS_GUID
		WHERE OPER_FLAG in (''U'',''X'')
    AND S.RX_DSPNSD_DT = H.RX_DSPNSD_DT
    AND S.SUPPLIER_PHARMACY_CD = H.SUPPLIER_PHARMACY_CD
    AND S.KEY_CLMNS_HASH = H.KEY_CLMNS_HASH
		)';

execute immediate 'INSERT INTO PS_RX_HIST ' || 
	' (TRANS_GUID,DETL_CLMNS_HASH,KEY_CLMNS_HASH,RX_DSPNSD_DT,SUPPLIER_PHARMACY_CD, DEL_IND) ' ||
	' SELECT S.TRANS_GUID,S.DETL_CLMNS_HASH,S.KEY_CLMNS_HASH,S.RX_DSPNSD_DT AS RX_DSPNSD_DT,S.SUPPLIER_PHARMACY_CD, S.DEL_IND ' ||
	' FROM ' || TBL_NM || ' S  
		JOIN ' || TBL_NM || '_H H ON S.TRANS_GUID = H.TRANS_GUID
		WHERE OPER_FLAG in (''N'',''U'',''X'')';

COMMIT;
end;
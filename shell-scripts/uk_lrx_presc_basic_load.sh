#!/bin/bash

# Script Name: uk_lrx_presc_basic_to_load.sh
# Release Version : 1.0
# Release Date : 15-10-2015
# Developed by Himanshu Ganatra
#
# This script is used to load the data from presc_basic file to database.

cd '/data/production/containers/dcp-prod/uk/uk_lrx/scripts/'
source '../config/uk_lrx.conf'

#Variables:
INDIR=$IN"uk_lrx_presc_basic_to_dcp/"
INFILE="presc_basic*"
LOGFILE=$LOGDIR"uk_lrx_prescriber_basic_DCP_load.log"
CTLFILE=$CONFIG"load_presc_basic.ctl"

PID=$$
CHECKDIR=`ls -1 $INDIR | wc -l`

DATETIME () {
date "+%Y-%m-%d %H:%M:%S";
}

#If not files exist in INDIR
if [ $CHECKDIR -eq 0 ]; then
echo "$(DATETIME) $PID ERROR: No files to process in $INDIR" >> $LOGFILE
exit 1
fi

for i in `ls $INDIR$INFILE`
{
FILENAME=$(basename "$i")
BADFILE=${FILENAME%.*}_unprocessed.dat
UNIQUE=${FILENAME%.*}_unique.dat
WRKFILE=${FILENAME%.*}".wrk"
DUPFILE=$FILENAME"_duplicates"
CTLLOGFILE=$LOGDIR$FILENAME"_ctl.log"

echo "Processing $FILENAME"

echo "$(DATETIME) $PID [$FILENAME] INFO: Processing $FILENAME" >> $LOGFILE

awk 'a[$0]++' $INDIR$FILENAME > $DUPDIR$DUPFILE   	#To seperate out the duplicate records
awk '!a[$0]++' $INDIR$FILENAME > $WRKDIR$UNIQUE	#To remove duplicate records from same file.

#awk 'BEGIN{FS=OFS=","}{$10="";gsub(FS "+",FS)}1' $WRKDIR$UNIQUE > $WRKDIR$WRKFILE

awk  'BEGIN { OFS=","; }  { nf=0; delete f; while ( match($0,/([^,]+)|(\"[^\"]+\")/) ) { f[++nf] = substr($0,RSTART,RLENGTH); $0 = substr($0,RSTART+RLENGTH); }; print f[1],f[2],f[3],f[4],f[5],f[6],f[7],f[8],f[9],f[11],"",f[12],f[13],f[14],f[15] }' $WRKDIR$UNIQUE | sed 's/,$//' > $WRKDIR$WRKFILE

#Inserting file information to PS_FILE and truncating the Staging table.

sqlplus $SQLPLUSCONN >$NULL 2>&1 <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_BAS_FILE_INFO('$FILENAME');
exit;
EOF

# If error occurs while running the Procedure.
if [ $? -ne 0 ]; then
echo "$(DATETIME) $PID [$FILENAME] ERROR: Error while inserting data into PS_FILE or Truncating PS_ANON_RXER_BAS_STG" >>$LOGFILE
exit 1
fi

FILE_ID=`sqlplus -silent $SQLPLUSCONN <<EOF
select max(SRC_FILE_ID) from PS_FILE;
EOF`

FILE_ID=$(sed "s/[^0-9]//g" <<<$FILE_ID)

#loading the data using SQLLDR
sqlldr $SQLLDRCONN control=$CTLFILE data=$WRKDIR$WRKFILE log=$CTLLOGFILE bad=$ERROR$BADFILE >/dev/null 2>&1

#if [ $? -ne 0 ]; then
#	echo "$LOGHEADER ERROR: Error in Loading Data to Staging table" >> $LOGFILE
#	continue
#fi

COUNT_DUP=`cat $DUPDIR$DUPFILE | wc -l`
COUNT_REC=`cat $WRKDIR$WRKFILE | wc -l`

echo "$(DATETIME) $PID [$FILENAME] INFO: Running Procedure to load data into target tables" >>$LOGFILE

PRCINFO=`sqlplus -s $SQLPLUSCONN >> $LOGFILE  <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_ANON_RXER_BAS_PRC($FILE_ID);
exit;
EOF`

RESULT=$?

PRCINFO=`echo "$PRCINFO" | tr '\n' '|' |sed 's/|/ | /g'| sed 's/ | $/"/g' | sed 's/^/"/g'`

if [ $RESULT -ne 0 ]; then
echo "$(DATETIME) $PID [$FILENAME] ERROR: $PRCINFO" >> $LOGFILE
cp $INDIR$FILENAME $PROCESSED
mv $INDIR$FILENAME $ERROR
rm $WRKDIR$WRKFILE
rm $WRKDIR$UNIQUE
continue
else 
echo "$(DATETIME) $PID [$FILENAME] INFO: Procedure Ran Successfully for $FILENAME" >> $LOGFILE
fi

echo "$(DATETIME) $PID [$FILENAME] INFO: File $FILENAME successfully processed; Total Transactions=$COUNT_REC; Duplicates Dropped=$COUNT_DUP" >> $LOGFILE

if [ $COUNT_DUP -eq 0 ]; then
rm -f $DUPDIR$DUPFILE
fi

mv $INDIR$FILENAME $PROCESSED
rm $WRKDIR$WRKFILE
rm $WRKDIR$UNIQUE
}

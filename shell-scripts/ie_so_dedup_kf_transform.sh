#!/bin/bash

# Script Name: ie_so_dedup_kf_transform.sh
# Release Version : 1.1
# Release Date : 05-10-2015
# Developed by Akhilesh Chand
# http://arc.imshealth.com/gm/folder-1.11.4759128
# Take the transaction file and load the data into database thereafter performing de-duplication and extract the data into kf expected format
# Input : Ireland Sell-out transaction input file(T_MCL_30404_20150317_020.000)
# Output : KF expected output file (LRXIEBD10_MCL_10658_W2015035_0167_00003510.TXT) or De-duplication transaction data.
# After performing De-duplicate if duplicate records come then moves into duplicate folder (T_MCL_30404_20150317_020.000_duplicates_20160201055042).

# source command can be used to load any functions file into the current shell script or a command prompt.
source /data/dcpdev/dev-staging-pflow/ie/ie_so/config/ie_so_dedup_kf_transform.conf
#source /data/dcpdev/dev-staging-pflow/ie/ie_so/config/ie_so_dedup_kf_transform.conf
#source ie_so_dedup_kf_transform.conf

#Variables information
#Date_Format=`date +%Y%m%d%H%M` # The output of Date_Format is like this YYYYmmDDHHMM(201602010209) YYYY->year mm->month DD->day of month HH->hour MM->minute
Script_Name=`basename $0 .sh`   # It returns name of script without .sh extension like this ie_so_dedup_kf_transform
Ire_Log_File="ie_so_dedup_kf_transform.log"

Ire_Ctl_Log_File="ie_so_deduplication_ctllog.log"

Ire_Temp_File="ie_so_deduplication_temp"

Process_Id=$$
# Date=`date "+%Y-%m-%d %H:%M:%S"`
Log_Type1="ERROR"
Log_Type2="DEBUG"
Log_Type3="INFO"

#Input/Output Directory information

Maindir=$DCPENV_PATH"ie/ie_so/"
File_Landing_Path=$Maindir"input/"
Duplicate_Dir=$Maindir"duplicates/"
UnProcessed=$Maindir"hold/"
Invalid_Dir=$Maindir"error/"
Dest_Landing_Path=$Maindir"output/"
Processed_Dir=$Maindir"processed/"
Left_Dir=$Maindir"left_panel/"
Wrk_Dir=$Maindir"wrkdir/"

#Log file and temp file information
Log_Dir=$DCPENV_PATH"ie/ie_so/log/"
Log_File=$Log_Dir$Ire_Log_File
Log_File_H=$Log_Dir$Ire_Log_File_H
Temp_File=$Wrk_Dir/$Ire_Temp_File.$$

Control_File=$Log_Dir/$Script_Name$$.ctl

Ctl_Log_File=$Wrk_Dir/$Ire_Ctl_Log_File


#Functions Definition

Date() { date "+%Y-%m-%d %H:%M:%S"; }           #This function return date with time like YYYY-MM-DD HH:MM:SS(2016-02-01 05:50:42)
Date_Format () { date "+%Y%m%d%H%M%S"; }
ChkFileExist()
        {
        if [ ! -f $1 ]; then
                touch $1
                chmod 666 $1
        fi
        }
# Check oracle is running or not.
ChkOracleEnv()
        {
                ENV_ORC=`env | grep ORACLE_HOME`
                ENV_ORC_VAL=$?
                if [ $ENV_ORC_VAL -ne 0 ]
                then

                        for i in "${ORACLE_ENV[@]}"; do

                                echo "$i" >> $BASH_STARTUP_FILE
                        done
                fi

        }

# It contains log info about every important operations.
Logmessage()
        {
        ChkFileExist $Log_File
        # [timestamp][process_id] [filename] [logging type] [log message]
        echo "$(Date) $Process_Id [$1] $2 : $3 " >> $Log_File

        }
# Check database is running or not.
DBConnection()
        {
                echo "exit" | sqlplus -L $DB_CONN | grep Connected > $NULL_FILE 2>&1
                if [ $? -eq 0 ]
                then
                        ChkFileExist $Log_File
                        echo "$(Date) $Process_Id  $Log_Type3 : Database is running" >> $Log_File

                else
                        ChkFileExist $Log_File
                        echo "$(Date) $Process_Id  $Log_Type1 : Database is not running" >> $Log_File
                        exit 1
                fi
        }

# Check sql errors
Sql_Error()
        {
                if [ $3 != 0 ]  #$3 contains the status of last execution of sql statement hereafter checking $3 is equal to 0 or not
                        then
                                # $1=[filename] $2=[logging type] $3=[log message]
                                Logmessage "$1" "$2" "$4"

                        exit 0; # Here code is exit becsuse sql statement return error.
                fi
        }
# Check directory is empty or not
ChkDirEmpty()
        {
        if find "$1" -maxdepth 0 -empty | read;
                then

                ChkFileExist $Log_File
                echo "$(Date) $Process_Id  $Log_Type3 : There is no file to process in input folder($1)." >> $Log_File
                exit 1
        fi
        }

De_Dup_SP_Status()
        {
                mv -f $File_name $3
                Sql_ARG3=`echo $2 |sed 's/ //g'`
                FILE_COUNT=`cat $Wrk_Dir$Sql_ARG3 | wc -l`
                if [ $FILE_COUNT -eq 1 ]
                        then
                                rm -rf $Wrk_Dir$Sql_ARG3

                fi

                FILE_COUNT=`cat $Duplicate_Dir$DUPLICATE_FILE | wc -l`

                if [ $FILE_COUNT -eq 0 ]
                then

                       rm -rf $Duplicate_Dir$DUPLICATE_FILE
                fi

                Logmessage "$File_name" "$Log_Type3" "$1 due to this file moved into $4 directory($3)."
                Logmessage "$File_name" "$Log_Type3" "De-Deduplication process is done."

Sql_conn=`sqlplus -s $DB_CONN <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
set feedback off
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name');
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name_H');
exit;
EOF`

                Sql_Err=$?
                Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                Logmessage "$File_name" "$Log_Type3"  "Dynamic table is removed."
                continue
        }

Rrecord_Info()
        {
        Loaded_Rec=`cat $Ctl_Log_File | grep "Rows successfully loaded" | tr -cd [:digit:]`
        Error_Rec=`cat $Ctl_Log_File | grep "Rows not loaded due to data errors" | tr -cd [:digit:]`
        Failed_Rec=`cat $Ctl_Log_File | grep "Rows not loaded because all WHEN clauses were failed" | tr -cd [:digit:]`
        Fields_Null_Rec=`cat $Ctl_Log_File | grep "Rows not loaded because all fields were null" | tr -cd [:digit:]`

        Logmessage "$File_name" "$Log_Type3" "Total records=$Temp_File_Cnt, Loaded records=$Loaded_Rec, Error records=$Error_Rec,Failed records=$Failed_Rec and Fields with null values= $Fields_Null_Rec ."
        Logmessage "$File_name" "$Log_Type3" "Renaming control file($Ctl_Log_File) to new control file($Log_Dir$CTL_LG_FLE) move into log directory."

        mv -f $Ctl_Log_File $Log_Dir$CTL_LG_FLE

        }
Change_Dir()
        {
        cd $1
        }
ChkOracleEnv
DBConnection
ChkDirEmpty $File_Landing_Path
Change_Dir $File_Landing_Path

# Passing input file one by one to variable
for File_name in `ls $File_Landing_Path`;
do

ChkDirEmpty $File_Landing_Path

# Checking the input file against regular expression
        echo "$File_name" | grep -q "^[A-Z]\_[A-Z]\{3\}\_[0-9]\{5,6\}\_[0-9]\{8\}\_[0-9]\{3\}\.[0-9]\{3\}$"

        if [ $? -eq 0 ] ;then

               Logmessage "$File_name" "$Log_Type3" "Input file($File_name) is ready to process."
#Reportlogs "start processing on $File_name"

# Create a new empty file to store invalid and duplicate records, if input file contains
               FILE_NM_SQLLDR=${File_name}.dat
               DUPLICATE_FILE=${File_name}_duplicates_$(Date_Format)
               CTL_LG_FLE=${File_name}_"ctl_log"_$(Date_Format)".log"
# Get the details from input file
               SUPPLIER_CD=`echo $File_name | cut -d '_' -f2`
               EXTRNL_PHARMACY_ID=`echo $File_name | cut -d '_' -f2,3`
               EXTRNL_PHARMACY_ID1=`cat $File_name | cut -d ',' -f3 | head -n 1`
               EXTRNL_PHARMACY_ID1=`echo $EXTRNL_PHARMACY_ID1 | sed 's/"//g'`
               Table_name=`echo $File_name | awk -F '_' '{print $1"_"$2"_"$3"_"$4}'`
               Table_Name=$Table_name$$
               Table_Name_H=$Table_name$$"_H"
               SUPPLIER_PHARAMCY_CD="$SUPPLIER_CD$EXTRNL_PHARMACY_ID1"

               PHARMACY_NM=`cat $File_name | head -n 1 | awk '{ nf=0; delete f; while ( match($0,/([^,]+)|(\"[^\"]+\")/) ) { f[++nf] = substr($0,RSTART,RLENGTH); $0 = substr($0,RSTART+RLENGTH); } print f[4] }'`
               PHARMACY_ADDR=`cat $File_name | head -n 1 | awk '{ nf=0; delete f; while ( match($0,/([^,]+)|(\"[^\"]+\")/) ) { f[++nf] = substr($0,RSTART,RLENGTH); $0 = substr($0,RSTART+RLENGTH); } print f[5] }'`


# Check the file is empty or not
               if [[ ! -s $File_name ]] ; then

                      mv -f $File_name $Invalid_Dir
                      Logmessage "$File_name" "$Log_Type1" "The size of input file($File_name) is zero."
                      Logmessage "$File_name" "$Log_Type1" "File moved into Error directory($Invalid_Dir)."
                      continue
               fi

# Check the number of line count in file is zero or not
                        FILE_COUNT=`cat $File_name | wc -l`

               if [[ $FILE_COUNT -eq 0 ]];
               then
                       mv -f $File_name $Invalid_Dir
                       Logmessage "$File_name" "$Log_Type1" "There is no record in input file($File_name)."
                       Logmessage "$File_name" "$Log_Type1" "File moved into Error directory($Invalid_Dir)."
                       continue
               fi
# Check data and header records is exist or not
               Header_Rec=`cat $File_name | sed -n '1p' | awk -F , '{ gsub(/"/,"",$1 ); print $1 }'`

               if [ "$Header_Rec" != "H" ]; then

                       mv -f $File_name $Invalid_Dir
                       Logmessage "$File_name" "$Log_Type1" "There is no header record in file hence moves to Error directory ($Invalid_Dir)."
                       continue
               fi

               Metadata=`cat $File_name | sed -n '2p' | awk -F , '{ gsub(/"/,"",$1); print $1 }'`

# Check metadata records is exist or not and set the flag_value
               if [ $Metadata == "M" ]; then
                        Flag=`cat $File_name | sed -n '2p' | awk -F , '{ gsub(/"/,"",$3); print $3 }'`

                        if [ $Flag == "1" ]; then
                                Flag_value=BD

                        elif [ $Flag == "0" ]; then
                                Flag_value=TR

                        else
                                mv -f $File_name $Invalid_Dir
                                Logmessage "$File_name" "$Log_Type1" "There is no flag value or incorrect flag value in meta data record due to this input file moved into Error directory ($Invalid_Dir)."
                                continue
                        fi
               else

                        mv -f $File_name $Invalid_Dir
                        Logmessage "$File_name" "$Log_Type1" "There is no meta data record in file hence moves to Error directory ($Invalid_Dir)."
                        continue
               fi
# Check data records is exist or not

                Data_Rec=`cat $File_name | sed -n '3p' | awk -F , '{ gsub(/"/,"",$1 ); print $1 }'`
                if [ "$Data_Rec" != "D" ]; then

                        mv -f $File_name $Invalid_Dir
                        Logmessage "$File_name" "$Log_Type1" "There is no transition record(data) in file hence moves to Error directory ($Invalid_Dir)."
                        continue

                fi

# Check metadata records is exist or not

                if [ $Metadata == "M" ]; then

                        cat $File_name | sed '1,2d' > $Temp_File
                fi

                TRAILER_VAL=`cat $Temp_File | sed -n '$p'`
                TRAILER_VAL=`echo $TRAILER_VAL | cut -d ',' -f1 | tr -d '"'`
#                TRAILER_VAL=`echo $TRAILER_VAL | tr -d '"'`

# Check Trailer record is exist or not
                if [[ "$TRAILER_VAL" == "T" ]];
                then
                       sed -i -e '$d' $Temp_File
                fi

Sql_conn=`sqlplus -silent $DB_CONN << EOF

WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
declare

OUTPUT_STATUS number(2);

begin

IRE_CHK_SHOP_ID_EXIST('$EXTRNL_PHARMACY_ID','$File_name',OUTPUT_STATUS);

end;
/
EOF`

                Sql_ARG1=`echo $Sql_conn | cut -d '|' -f1 | sed 's/\"//'`
                Sql_ARG2=`echo $Sql_conn | cut -d '|' -f2 | sed 's/\"//'`
                Sql_ARG3=`echo $Sql_conn | cut -d '|' -f3 | sed 's/\"//'`

                if [[ $Sql_ARG1 == 1 ]] ; then

                        Logmessage "$File_name" "$Log_Type1" "$Sql_ARG2"
                        #mv -f $File_name $UnProcessed
                        mv -f $File_name $UnProcessed

                        Logmessage "$File_name" "$Log_Type1" "Input file($File_name) moved into hold folder($UnProcessed)."
                        rm -f  $Temp_File
                        continue
                elif [[ $Sql_ARG1 == 0 ]] ; then


                        if [[ "$Sql_ARG3" == "NEW" ]]; then

                        Logmessage "$File_name" "$Log_Type3" "$Sql_ARG2"


                        elif [[ "$Sql_ARG3" == "LEFT PANEL"  ]]; then

                        Logmessage "$File_name" "$Log_Type3" "Input file($File_name) moved into left panel folder"
                        mv -f $File_name $Left_Dir
                        rm -f $Temp_File
                        continue

                        fi
                fi



# Check control file is exist or not
                if [  -e $Control_File ]; then

                        rm -f $Control_File
                        touch $Control_File

                fi

                if [ ! -e $Invalid_Dir$FILE_NM_SQLLDR ]; then

                        touch $Invalid_Dir$FILE_NM_SQLLDR
                        chmod 666 $Invalid_Dir$FILE_NM_SQLLDR
                fi


                if [ !  -e $Ctl_Log_File ]; then

                        touch $Ctl_Log_File
                fi

# Create dynamic table and store the column and datatype into control file
                #echo "OPTIONS (SKIP=2)" >> $Control_File
                echo "load data" >> $Control_File
                echo "CHARACTERSET UTF8" >> $Control_File
                echo "into table $Table_Name" >> $Control_File
                echo fields terminated by "\",\"" optionally enclosed by "'\"'" TRAILING NULLCOLS >> $Control_File

                echo "(REC_TYP,RX_TYP,RX_DSPNSD_DT date 'YYYYMMDD' ,RX_DSPNSD_TM,RX_REPEAT_STATUS,EXMT_STATUS,RX_ID,RX_ITEM_SEQ,SUPPLIERS_DSPNSD_DRUG_CD,DSPNSD_DRUG_IPU_CD,DSPNSD_DRUG_DESC,GENERIC_USE_MARKER,DSPNSD_QTY,DSPNSD_UNIT_OF_QTY,DSPNSD_DRUG_PACK_SIZE,SUPPLIERS_PSCR_DRUG_CD,PSCR_DRUG_IPU_CD,PSCR_DRUG_DESC,PSCR_QTY,VERBOSE_DOSAGE CHAR(1024),COST_OF_DSPNSD_QTY,NRSG_HM_IND,DEL_IND,TRANS_GUID,SUPPLIER_CD constant $SUPPLIER_CD,EXTRNL_PHARMACY_ID constant $EXTRNL_PHARMACY_ID,DETL_CLMNS_HASH \"ORA_HASH (:RX_TYP||:RX_REPEAT_STATUS||:EXMT_STATUS||:SUPPLIERS_DSPNSD_DRUG_CD||:DSPNSD_DRUG_IPU_CD||:GENERIC_USE_MARKER||:DSPNSD_QTY||:DSPNSD_UNIT_OF_QTY||:DSPNSD_DRUG_PACK_SIZE||:SUPPLIERS_PSCR_DRUG_CD||:PSCR_DRUG_IPU_CD||:PSCR_QTY||:COST_OF_DSPNSD_QTY||:NRSG_HM_IND )\",KEY_CLMNS_HASH \"ORA_HASH(:SUPPLIER_CD||:EXTRNL_PHARMACY_ID||:RX_ID||:RX_ITEM_SEQ||:RX_DSPNSD_DT)\",SUPPLIER_PHARMACY_CD constant $SUPPLIER_PHARAMCY_CD,PHARMACY_NM constant $PHARMACY_NM,PHARMACY_ADDR constant $PHARMACY_ADDR)" >> $Control_File


                chmod 777 $Control_File $Ctl_Log_File
                Logmessage "$File_name" "$Log_Type3" "Control files($Control_File) are created in runtime."

# Stored Procedure check whether table is exist or not, if exist then delete it thereafter recreate


Sql_conn=`sqlplus -silent $DB_CONN << EOF

WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
exec IRE_CHECKTABLEEXIST('$Table_Name');
exec IRE_CHECKTABLEEXIST_H('$Table_Name_H');
exit;
EOF`
                Sql_Err=$?
                Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"

# Through sql loader , load the data into dynamic table
                Logmessage "$File_name" "$Log_Type3" "Sql loader is loading data into dynamic table."
                sqlldr "$DB_CONN_SQL_LOADER" control=$Control_File log=$Ctl_Log_File data=$Temp_File bad=$Invalid_Dir$FILE_NM_SQLLDR ERRORS=999999999 > $NULL_FILE  2>&1

                RESULT=$?
                if [ $RESULT -ne 0 ] && [ $RESULT -ne 2 ];
                then

                        Logmessage "$File_name" "$Log_Type1" "The sql loader call failed."
                        Temp_File_Cnt=`cat $Temp_File | wc -l`

                        Rrecord_Info
                        Logmessage "$File_name" "$Log_Type3" "Input file($File_name) moved into error records($Invalid_Dir)."
                        mv -f $File_name $Invalid_Dir
                        rm -f $Temp_File $Control_File
Sql_conn=`sqlplus -s $DB_CONN <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
set feedback off
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name');
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name_H');
exit;
EOF`

                Sql_Err=$?
                Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                Logmessage "$File_name" "$Log_Type3"  "Dynamic table is removed."
                continue
                fi


                Bd_File_Cnt=`cat $Invalid_Dir$FILE_NM_SQLLDR | wc -l`
                Temp_File_Cnt=`cat $Temp_File | wc -l`

                if [ $Bd_File_Cnt -eq $Temp_File_Cnt ];
                then
                        Logmessage "$File_name" "$Log_Type1" "All records are rejected by sql loader due of incorrect/invalid records in input file."
                        Rrecord_Info
                        Logmessage "$File_name" "$Log_Type3" "Input file($File_name) moved into error records($Invalid_Dir)."
                        mv -f $File_name $Invalid_Dir
                        rm -f $Temp_File $Control_File
Sql_conn=`sqlplus -silent $DB_CONN << EOF

WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
exec IRE_CHECKTABLEEXIST('$Table_Name');
exec IRE_CHECKTABLEEXIST_H('$Table_Name_H');
exit;
EOF`
                Sql_Err=$?
                Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                continue
                fi


                if [ -e $Invalid_Dir$FILE_NM_SQLLDR ];
                then
                        mv -f $Invalid_Dir$FILE_NM_SQLLDR $Invalid_Dir$File_name
                fi

                Logmessage "$File_name" "$Log_Type3"  "Sql loader loaded the data into dynamic table($Table_Name)."
                Rrecord_Info

                rm -f $Temp_File $Control_File
                Logmessage "$File_name" "$Log_Type3" "Temporary file($Temp_File) is removed."
                Logmessage "$File_name" "$Log_Type3" "De-Deduplication process is start."

#echo "$EXTRNL_PHARMACY_ID,$DUPLICATE_FILE,'$Table_Name','$SUPPLIER_CD',EXT_PHARMA_ID,'$Flag_value','$Bd_File_Cnt',OUTPUT_STATUS,DUP_FILE_NAME,'$DE_DUP_WRKDIR','$DE_DUP_DUPLICATES_NAME','$Table_Name_H'"
#exit 1
#De-Deduplication process is start

Sql_conn=`sqlplus -s $DB_CONN <<EOF
set serveroutput on
set termout off
set feedback off

declare

EXT_PHARMA_ID varchar2(10) := '$EXTRNL_PHARMACY_ID';
DUP_FILE_NAME varchar2(100) := '$DUPLICATE_FILE';
OUTPUT_STATUS number(2);

begin

IRE_DE_DUP_PROC('$Table_Name','$SUPPLIER_CD',EXT_PHARMA_ID,'$Flag_value','$Bd_File_Cnt',OUTPUT_STATUS,DUP_FILE_NAME,'$DE_DUP_WRKDIR','$DE_DUP_DUPLICATES_NAME','$Table_Name_H');

end;
/
EOF`


#De-Deduplication process is end

                Sql_ARG1=`echo $Sql_conn | cut -d '|' -f1 | sed 's/\"//'`
                Sql_ARG2=`echo $Sql_conn | cut -d '|' -f2 | sed 's/\"//'`
                Sql_ARG3=`echo $Sql_conn | cut -d '|' -f3 | sed 's/\"//'`


                if [[ $Sql_ARG1 == 0 ]] ; then

                        FILE_COUNT=`cat $Wrk_Dir$Sql_ARG3 | wc -l`
                        FILE_COUNT=`expr $FILE_COUNT - 1`

                        Var_A=`echo "$Sql_ARG2" |cut -d'_' -f6 | cut -d'.' -f1`
                        Var_B=`printf "%07d" $FILE_COUNT`
                        Var_File=`echo $Sql_ARG2 | sed s/$Var_A.TXT/$Var_B.TXT/g`

                        mv -f $Wrk_Dir$Sql_ARG3 $Dest_Landing_Path$Var_File

                        FILE_COUNT=`cat $Duplicate_Dir$DUPLICATE_FILE | wc -l`

                        if [ $FILE_COUNT -eq 0 ]
                        then

                                rm -rf $Duplicate_Dir$DUPLICATE_FILE
                        fi

                        Logmessage "$File_name" "$Log_Type3" "De-duplication process is done successfully."
                        Logmessage "$File_name" "$Log_Type3" "Input file($File_name) successfully transform to KF input file($Var_File) format."
                        mv -f "$File_name" "$Processed_Dir"
                        Logmessage "$File_name" "$Log_Type3" "Input file($File_name) moved into Processed folder($Processed_Dir)."
                        Logmessage "$File_name" "$Log_Type3" "Records are loading into History table(ps_rx_hist)."

#Stored procedure Load the unique data into history(ps_rx_hist) table
Sql_conn=`sqlplus -silent $DB_CONN <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
exec IRE_PS_LOAD_RX_HIST('$Table_Name');
exit;
EOF`

                        Sql_Err=$?
                        Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                        Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                        Logmessage "$File_name" "$Log_Type3"  "Records succesfully loaded into History table."
                        Logmessage "$File_name" "$Log_Type3"  "Dyanmic table($Table_Name) is droping."

#Stored procedure drop the dynamic table
Sql_conn=`sqlplus -silent $DB_CONN <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name');
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name_H');
exit;
EOF`

                        Sql_Err=$?
                        Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                        Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                        Logmessage "$File_name" "$Log_Type3"  "Dynamic table is removed."

                        Logmessage "$File_name" "$Log_Type3" "Successfully completed file processing for $File_name ."

                elif [[ $Sql_ARG1 == 1  ]] ; then                       # Duplicate Records
                        mv -f $File_name $DUPLICATE_FILE
                        mv -f "$DUPLICATE_FILE" $Duplicate_Dir
                        Sql_ARG3=`echo $Sql_ARG3 |sed 's/ //g'`
                        FILE_COUNT=`cat $Wrk_Dir$Sql_ARG3 | wc -l`
                        if [ $FILE_COUNT -eq 1 ]
                        then

                                rm -rf $Wrk_Dir$Sql_ARG3

                        fi

                        Logmessage "$File_name" "$Log_Type3" "$Sql_ARG2 due to this file moved into Duplicate directory($Duplicate_Dir) with new naming convenstion($DUPLICATE_FILE)."
                        Logmessage "$File_name" "$Log_Type3" "De-Deduplication process is done successfully."
Sql_conn=`sqlplus -s $DB_CONN <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
set feedback off
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name');
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name_H');
exit;
EOF`

                        Sql_Err=$?
                        Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                        Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                        Logmessage "$File_name" "$Log_Type3"  "Dynamic table is removed."

                        continue

                elif [[ $Sql_ARG1 == 2  ]] ; then               # Ignore Records

                De_Dup_SP_Status "$Sql_ARG2" "$Sql_ARG3" "$Invalid_Dir" "error"

                elif [[ $Sql_ARG1 == 3  ]] ; then               # There is no records in dynamic table

                De_Dup_SP_Status "$Sql_ARG2" "$Sql_ARG3" "$UnProcessed" "hold"

                elif [[ $Sql_ARG1 == 4  ]] ; then               # Application Error comes from stored procedure

                De_Dup_SP_Status "$Sql_ARG2" "$Sql_ARG3" "$UnProcessed" "hold"

                elif [[ $Sql_ARG1 == 5  ]] ; then               # Records are duplicate and Ignored

                De_Dup_SP_Status "$Sql_ARG2" "$Sql_ARG3" "$Duplicate_Dir$DUPLICATE_FILE" "duplicate/hold"

                else

                        Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                        echo "$(Date) $Process_Id [$File_name] $Log_Type1 : $Sql_conn " >> $Log_File
                        mv -f $File_name $UnProcessed
                        Logmessage "$File_name" "$Log_Type3" "De-Deduplication process is failed due to this input file($File_name) moved to hold folder($UnProcessed)."

#Stored procedure drop the dynamic table
Sql_conn=`sqlplus -silent $DB_CONN <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name');
exec IRE_DE_DUP_DROP_DY_TABLE('$Table_Name_H');
exit;
EOF`
                        Sql_Err=$?
                        Sql_conn=`echo "$Sql_conn" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`
                        Sql_Error "$File_name" "$Log_Type1" "$Sql_Err" "$Sql_conn"
                        Logmessage "$File_name" "$Log_Type3"  "Dynamic table is removed."


                fi

else
# Input file contains invlaid records and moves into invalid directory.
        mv -f $File_name $Invalid_Dir
        Logmessage "$File_name" "$Log_Type1" "It is an invalid file name($File_name) and moved into Error directory($Invalid_Dir)."
        continue
fi
done
exit 0
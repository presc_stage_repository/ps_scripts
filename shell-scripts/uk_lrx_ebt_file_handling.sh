#!/bin/bash

cd '/data/production/containers/dcp-prod/uk/uk_lrx/scripts/'

source '../config/uk_lrx.conf'

INDIR=$IN"uk_lrx_ebt_to_dcp/"
LOGFILE=$LOGDIR"uk_lrx_ebt_to_dcp.log"
TEMP_FILE=$WRKDIR"TEMP_FILE$$"
TEMP_FILE1=$WRKDIR"TEMP_FILE1$$"
OUTPUT=$OUT"uk_lrx_dcp_to_tfp/"
DATE=`date +%Y%m%d`
PID=$$
LOG_TYPE1="ERROR"
LOG_TYPE2="DEBUG"
LOG_TYPE3="INFO"
TAILER="T"

DATETIME () {
date "+%Y-%m-%d %H:%M:%S";
}

ChkOracleEnv()
        {
                ENV_ORC=`env | grep ORACLE_HOME`
                ENV_ORC_VAL=$?
                if [ $ENV_ORC_VAL -ne 0 ]
                then
                        for i in "${ORACLE_ENV[@]}"; do
                                echo "$i" >> $BASH_STARTUP_FILE
                        done
                fi
        }

DBConnection()
        {
                echo "exit" | sqlplus -L $SQLPLUSCONN | grep Connected > $NULL 2>&1
                if [ $? -eq 0 ]
                then
                         touch $LOGFILE
                         chmod 777 $LOGFILE
                         echo "$(DATETIME) $PID  $LOG_TYPE3 : Database is running" >> $LOGFILE

                else
                         touch $LOGFILE
                         chmod 777 $LOGFILE
                         echo "$(DATETIME) $PID $LOG_TYPE1 : Database is not running" >> $LOGFILE
                         exit
                fi
        }

ChkDirEmpty()
        {
        if find "$1" -maxdepth 0 -empty | read;
                then
                touch $LOGFILE
                chmod 777 $LOGFILE
                echo "$(DATETIME) $PID $LOG_TYPE3 : There is no file to process in input folder($1) " >> $LOGFILE
                exit
        fi
        }

Logmessage()
        {
        touch $LOGFILE
        chmod 777 $LOGFILE
        echo "$(DATETIME) $PID [$1] $2 : $3 " >> $LOGFILE
        }

ChkOracleEnv
DBConnection
ChkDirEmpty $INDIR

#FILE=`ls $INDIR`

#echo $test1
#exit 1

for FILE_NAME in `ls $INDIR`;
do

#Input file like this "GB9_DCP_COHL_20150531.csv"
	UNPROCESSED_FILE=${FILE_NAME}_unprocessed
	INVALID_FILE=${FILE_NAME}_invalid


	echo "$FILE_NAME" | grep -q "^GB9\_DCP\_[A-Z]\{4\}\_[0-9]\{8\}\.csv$"

	if [ $? -eq 0 ]
	then
	

	#echo $INDIR$FILE_NAME	
		CODE=`echo "$FILE_NAME" | cut -d '_' -f3`

			if [ "$CODE" == "MARL" ]
                        then
                                 Logmessage "$FILE_NAME" "$LOG_TYPE3" "Input file($FILE_NAME) is ready to process"
                        #cat GB9_DCP_COHL_20150531.csv | head -1
                                HEADER_FIRST_FIELD=`cat $INDIR$FILE_NAME | awk -F',' '{ OFS=","; print $1}' | head  -1 | sed -e 's/"//g'`
                                REFORMAT_FILE_NAME=$CODE$HEADER_FIRST_FIELD.dat.asc
                                cat $INDIR$FILE_NAME | awk -F',' -v HEADER="H" '{ OFS=","; print HEADER,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' | head  -1 | sed -e 's/"//g' > $WRKDIR$REFORMAT_FILE_NAME
                                cat $INDIR$FILE_NAME | awk -F',' -v DATA="D" '{ OFS=","; print DATA,$11,$12,$13,$14,$15 }' | sed -e 's/"//g' >> $WRKDIR$REFORMAT_FILE_NAME
                                DATA_COUNT=`cat $INDIR$FILE_NAME | awk -F',' '{ OFS=","; print $11,$12,$13,$14,$15 }' | wc -l`
                                echo "$TAILER,$DATA_COUNT" >> $WRKDIR$REFORMAT_FILE_NAME

                                mv -f $INDIR$FILE_NAME $PROCESSED
                                Logmessage "$FILE_NAME" "$LOG_TYPE3" "Input file($FILE_NAME) has moved into processed folder"

                                mv -f $WRKDIR$REFORMAT_FILE_NAME $OUTPUT
                                Logmessage "$FILE_NAME" "$LOG_TYPE3" "Output file($REFORMAT_FILE_NAME) generated successfully"

`sqlplus -s $SQLPLUSCONN >$NULL 2>&1 <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_FILE_INFO_PROC('$REFORMAT_FILE_NAME','$FILE_NAME');
exit;
EOF`

			elif [ "$CODE" == "COHL" ]
			then
				Logmessage "$FILE_NAME" "$LOG_TYPE3" "Input file($FILE_NAME) is ready to process"
			#cat GB9_DCP_COHL_20150531.csv | head -1
				HEADER_FIRST_FIELD=`cat $INDIR$FILE_NAME | awk -F',' '{ OFS=","; print $1}' | head  -1 | sed -e 's/"//g'`
				REFORMAT_FILE_NAME=$CODE$HEADER_FIRST_FIELD.dat.asc
				cat $INDIR$FILE_NAME | awk -F',' -v HEADER="H" '{ OFS=","; print HEADER,$1,$2,$3,$4,$5,$6,$7,$8,$9,$10}' | head  -1 | sed -e 's/"//g' > $WRKDIR$REFORMAT_FILE_NAME
				cat $INDIR$FILE_NAME | awk -F',' -v DATA="D" '{ OFS=","; print DATA,$11,$12 }' | sed -e 's/"//g' >> $WRKDIR$REFORMAT_FILE_NAME	
				DATA_COUNT=`cat $INDIR$FILE_NAME | awk -F',' '{ print $11,$12 }' | wc -l`
				echo "$TAILER,$DATA_COUNT" >> $WRKDIR$REFORMAT_FILE_NAME
				
				mv -f $INDIR$FILE_NAME $PROCESSED
				Logmessage "$FILE_NAME" "$LOG_TYPE3" "Input file($FILE_NAME) has moved into processed folder"

				mv -f $WRKDIR$REFORMAT_FILE_NAME $OUTPUT 	
				Logmessage "$FILE_NAME" "$LOG_TYPE3" "Output file($REFORMAT_FILE_NAME) generated successfully"
				
`sqlplus -s $SQLPLUSCONN >$NULL 2>&1 <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_FILE_INFO_PROC('$REFORMAT_FILE_NAME','$FILE_NAME');
exit;
EOF`	


			else
				mv -f $INDIR$FILE_NAME $WRKDIR$UNPROCESSED_FILE
				mv -f $WRKDIR$UNPROCESSED_FILE $HOLDDIR 
				Logmessage "$FILE_NAME" "$LOG_TYPE1" "It is an invalid file name($FILE_NAME) due to this moved into Error directory($HOLDDIR)."

			fi
	else
		mv -f $INDIR$FILE_NAME $WRKDIR$UNPROCESSED_FILE
		mv -f $WRKDIR$UNPROCESSED_FILE $HOLDDIR
		Logmessage "$FILE_NAME" "$LOG_TYPE1" "It is an invalid file name($FILE_NAME) and moved into Error directory($HOLDDIR)."
        fi

done

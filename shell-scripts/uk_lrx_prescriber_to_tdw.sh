#!/bin/bash

# Script Name: uk_lrx_prescriber_to_tdw.sh
# Release Version : 1.0
# Release Date : 15-10-2015
# Developed by Himanshu Ganatra
#
# This script is used to extract prescriber data from database.

cd '/data/production/containers/dcp-prod/uk/uk_lrx/scripts/'
source '../config/uk_lrx.conf'

LOGFILE=$LOGDIR"uk_lrx_prescriber_to_tdw.log"
OUTFILE=$TDW_OUT"gb9_dcp_rxerbureau_"`date +"%Y%m%d"`".dat"
FILENM=`basename $OUTFILE`
EVENTFILE=$TDW_OUT"gb9_dcp_rxerbureau_"`date +"%Y%m%d"`".event"

PID=$$
DATETIME () {
date "+%Y-%m-%d %H:%M:%S";
}

echo "$(DATETIME) $PID [$FILENM] INFO: Generating $FILENM" >> $LOGFILE
echo "Site ID,Earliest Date,Latest Date,Record Count,No of Days,Data Received">$OUTFILE

#Extracting Data
sqlplus -silent $SQLPLUSCONN > $NULL 2>&1 << EOF

SET linesize 2500 trims on pages 0 feed off
SET TRIMSPOOL ON
SET TRIMOUT ON
SET TERMOUT OFF
SPOOL $OUTFILE APPEND

select /*+ use_hash (s t slm sl tlm tl sp p) */s.EXTRNL_ID || chr(1) ||
t.EXTRNL_ID || chr(1) ||
p.intr_pharmacy_id || chr(1) ||
tl.EXTRNL_LOC_ID|| chr(1) ||
'1800-01-01'  || chr(1) ||
'2999-01-01' as ref_data from
(SELECT /*+ use_hash (a m) */ a.anon_rxer_id,a.rxer_typ_cd,a.anon_typ_cd,a.src_file_id, a.extrnl_id, m.trgt_anon_rxer_id,
row_number() over (partition by a.extrnl_id order by m.eff_dt_fr desc) rn FROM ps_anon_rxer a
left join ps_anon_rxer_map m on a.anon_rxer_id = m.anon_rxer_id
where m.eff_dt_to is null and a.ANON_TYP_CD = 'SEQUENCE')
s
left join ps_anon_rxer t on t.anon_rxer_id = s.trgt_anon_rxer_id
left join ps_anon_rxer_ps_loc_grp slm on s.anon_rxer_id = slm.anon_rxer_id
left join ps_loc_grp sl on sl.loc_grp_id = slm.loc_grp_id
left join ps_anon_rxer_ps_loc_grp tlm on t.anon_rxer_id = tlm.anon_rxer_id
left join ps_loc_grp tl on tl.loc_grp_id = tlm.loc_grp_id
left join ps_anon_rxer_ps_pharmacy sp on s.anon_rxer_id = sp.anon_rxer_id
left join ps_pharmacy p on sp.surro_pharmacy_id = p.surro_pharmacy_id
where s.rn = 1;

WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;

SPOOL OFF

inset into PS_FILE values (SRC_FILE_ID_SEQ.NEXTVAL, '$FILENM', sysdate,'1');
commit;

EOF

if [ $? -ne 0 ]; then
echo "$(DATETIME) $PID [$FILENM] ERROR: An error Occurred while extracting data from database for $FILENM" >>$LOGFILE
exit 1
fi

touch $EVENTFILE
echo "$(DATETIME) $PID [$FILENM] INFO: File $FILENM Created Successfully" >> $LOGFILE

#!/bin/bash

# Script Name: us_lrx_cohort_cresp_to_tdw.sh
# Release Version : 1.0
# Release Date : 15-10-2015
# Developed by Himanshu Ganatra
# This script is used to generate output file of cohorts for TDW by processing CRES File which comes from Bureau

cd '/data/production/containers/dcp-prod/uk/uk_lrx/scripts/'
source '../config/uk_lrx.conf'

INDIR=$IN"uk_lrx_cresp_to_dcp/"
INFILE="CRES*"
LOGFILE=$LOGDIR"uk_lrx_cres_to_dcp.log"
DATE=`date +%Y%m%d`
LOGDATE=`date +%Y-%m-%d`
TIME=`date +%H:%M:%S`
PID=$$
CHECKDIR=`ls -1 $INDIR | wc -l`

# Log and exit if INDIR is empty

if [ $CHECKDIR -eq 0 ]; then
echo "$LOGDATE $TIME ERROR: No files to process in $INDIR" >> $LOGFILE
exit 1
fi

for i in `ls $INDIR$INFILE`
{
FILENAME=$(basename "$i")
FILENAME_WE=${FILENAME%.*}      #Filename wihtout extension
WRKFILE=$FILENAME_WE".wrk"
COHORT_SET=`echo $FILENAME | cut -d '_' -f1 | sed "s/[^0-9]//g"`
CLIENT_NUMBER=`echo $COHORT_SET | cut -c1-3`
PROJECT_NUMBER=`echo $COHORT_SET | cut -c4-5`
FILEDATE=`echo $FILENAME | cut -d '_' -f2 | sed "s/[^0-9]//g"`
COHORT_CONTENT="gb9_dcp_rxercohtcntnt_"$DATE".dat"
COHORT_CMPLT="gb9_dcp_rxercoht_"$DATE".cmplt"
COHORT_DEFINITION="gb9_dcp_rxercoht_"$DATE".dat"
LOGHEADER="$LOGDATE $TIME $PID [$FILENAME]"

#Checking if file is from DDMS or from LO

if [ $COHORT_SET -eq 00000 ];then
echo "$LOGHEADER INFO: Processing DDMS File $FILENAME" >> $LOGFILE
DDMS=1
else
echo "$LOGHEADER INFO: Processing CREQ File $FILENAME" >> $LOGFILE
DDMS=0
fi

#Generating Cohort Content file

tail -n +2 $INDIR$FILENAME > $WRKDIR$WRKFILE

awk -v COHORT_SET=$COHORT_SET -F"," '{print COHORT_SET"\x01"$2"\x01"$1"\x01""GB""\x01""1800-01-01""\x01""2999-01-01"}' $WRKDIR$WRKFILE > $TDW_OUT$COHORT_CONTENT

if [ $? -ne 0 ];then
echo "$LOGHEADER ERROR: Generating Cohort Content file Failed" >> $LOGFILE
cp $INDIR$FILENAME $UNPROCESSED$FILENAME
rm $INDIR$FILENAME
rm $WRKDIR$WRKFILE
continue
else
echo "$LOGHEADER INFO: Cohort Content File ($COHORT_CONTENT) generated Successfully" >> $LOGFILE
fi

#Generating Cohort definition file

if [ $DDMS -eq 0 ]; then
awk -v COHORT_SET=$COHORT_SET -v CLIENT_NUMBER=$CLIENT_NUMBER -F"," '{print $2"\x01"COHORT_SET"\x01""CSET"COHORT_SET"\x01""C"$2"\x01""LO""\x01"CLIENT_NUMBER"\x01""GB""\x01""1800-01-01""\x01""2999-01-01"}'  $WRKDIR$WRKFILE | uniq > $TDW_OUT$COHORT_DEFINITION
else
awk -v COHORT_SET=$COHORT_SET -v CLIENT_NUMBER=$CLIENT_NUMBER -F"," '{print $2"\x01"COHORT_SET"\x01""CSET"COHORT_SET"\x01""C"$2"\x01""DDMS""\x01"CLIENT_NUMBER"\x01""GB""\x01""1800-01-01""\x01""2999-01-01"}' $WRKDIR$WRKFILE | uniq > $TDW_OUT$COHORT_DEFINITION 
fi


if [ $? -ne 0 ];then
echo "$LOGHEADER ERROR: Generating Cohort Definition File Failed" >> $LOGFILE
cp $INDIR$FILENAME $UNPROCESSED$FILENAME
rm $INDIR$FILENAME
rm $WRKDIR$WRKFILE
else
cp $INDIR$FILENAME $PROCESSED$FILENAME
rm $INDIR$FILENAME
rm $WRKDIR$WRKFILE
fi

#Creating .event and .cmplt file.

touch $TDW_OUT${COHORT_DEFINITION%.*}".event"
touch $TDW_OUT${COHORT_CONTENT%.*}".event"
touch $TDW_OUT$COHORT_CMPLT
echo "$LOGHEADER INFO: Cohort Definition file generated successfully" >> $LOGFILE
}

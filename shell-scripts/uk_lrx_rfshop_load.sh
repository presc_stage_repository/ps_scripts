#!/bin/bash

# Script Name: uk_lrx_rfshop_to_load.sh
# Release Version : 1.0
# Release Date : 5-11-2015
# Developed by Himanshu Ganatra
#
# This script is used to load the data from rfshop.dat file to database.

cd '/data/dcpdev/dev-staging-pflow/uk/uk_lrx/scripts/'

source '../config/uk_lrx.conf'

INDIR=$IN"uk_lrx_rfshop_to_dcp/"
#INFILE="rfshop*"
INFILE="*"

LOGFILE=$LOGDIR"uk_lrx_rfshop_DCP_load.log"
CTLLOGFILE=$LOGDIR"uk_lrx_rfshop_DCP_ctl.log"

CTLFILE=$CONFIG"load_rfshop.ctl"
TABLE_NAME="PS_PHARMACY_STG"
DATETIME() {
date "+%Y-%m-%d %H:%M:%S";
}
DATE=`date "+%Y%m%d%H%M%S"`
PID=$$
CHECKDIR=`ls -1 $INDIR | wc -l`

#If not files exist in INDIR
if [ $CHECKDIR -eq 0 ]; then
echo "$(DATETIME) $PID ERROR: No files to process in $INDIR" >> $LOGFILE
exit 0
fi

for i in `ls $INDIR$INFILE`
do
FILENAME=$(basename "$i")
FILENAME1=$(basename "$i" .dat)
TEMP_FILE=$FILENAME1$$
TEMP_FILE1=$FILENAME1"_"$$
BADFILE=${FILENAME%.*}"_unprocessed.dat"
UNIQUE=${FILENAME%.*}"_unique.dat"
WRKFILE=$FILENAME1"_1"$$".dat"
DUPFILE=$FILENAME$$"_duplicates"
DESTFILE=$FILENAME1"_""$DATE"".dat"

echo "$(DATETIME) $PID [$FILENAME] INFO: File($FILENAME) is ready to process" >>$LOGFILE

HEADER_VAL=`cat $i | head -n 1 | cut -d '/' -f1 | cut -d ':' -f1`

if [ "$HEADER_VAL" == "HEADER" ]
then
        cat $i | sed '1d' > $WRKDIR$TEMP_FILE
        chmod 777 $WRKDIR$TEMP_FILE
else
        cat $i > $WRKDIR$TEMP_FILE
        chmod 777 $WRKDIR$TEMP_FILE
fi

TRAILER_VAL=`cat $i  | tail -n 1 | cut -d ':' -f1`

if [ "$TRAILER_VAL" == "TRAILER" ]
then
        cat $WRKDIR$TEMP_FILE | sed '$d' > $WRKDIR$TEMP_FILE1
        chmod 777 $WRKDIR$TEMP_FILE1
else
        cat $WRKDIR$TEMP_FILE > $WRKDIR$TEMP_FILE1
        chmod 777 $WRKDIR$TEMP_FILE1

fi


touch $HOLDDIR$BADFILE $PROCESSED$DESTFILE
awk 'a[$0]++' $WRKDIR$TEMP_FILE1 > $DUPDIR$DUPFILE      #To seperate out the duplicate records
awk '!a[$0]++' $WRKDIR$TEMP_FILE1 > $WRKDIR$WRKFILE     #To remove duplicate records from same file.
chmod 777 $WRKDIR$WRKFILE $HOLDDIR$BADFILE $CTLLOGFILE $CTLFILE $PROCESSED$DESTFILE
F_COUNT=`cat $WRKDIR$WRKFILE | wc -l`
D_COUNT=`awk -F"~" 'match($18,/^ACT$|^LFP$|^EPO$|^ONTES$/) {print $18}' $WRKDIR$WRKFILE | wc -l`

if [ $F_COUNT != $D_COUNT ];
then
        mv -f $INDIR$FILENAME $ERROR
        rm -f $WRKDIR$WRKFILE $HOLDDIR$BADFILE $PROCESSED$DESTFILE
        echo "$(DATETIME) $PID [$FILENAME] ERROR: Panel status contains invalid value due to this file move to error folder";
        echo "$(DATETIME) $PID [$FILENAME] INFO : Script is exited";
        exit 1
fi

#Inserting file information to PS_FILE and truncating the Staging table.

sqlplus $SQLPLUSCONN >$NULL 2>&1 <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_RFSHOP_FILE_INFO('$FILENAME');
exit;
EOF

if [ $? -ne 0 ]; then
echo "$(DATETIME) $PID [$FILENAME] ERROR: Error while inserting data into PS_FILE or Truncating PS_PHARMACY_STG" >>$LOGFILE
exit 1
fi

FILE_ID=`sqlplus -silent $SQLPLUSCONN <<EOF
select max(SRC_FILE_ID) from PS_FILE;
EOF`

FILE_ID=$(sed "s/[^0-9]//g" <<<$FILE_ID)

#loading the data using SQLLDR
sqlldr $SQLLDRCONN control=$CTLFILE log=$CTLLOGFILE data=$WRKDIR$WRKFILE bad=$HOLDDIR$BADFILE >$NULL 2>&1

RESULT=$?
                        if [ $RESULT -ne 0 ] && [ $RESULT -ne 2 ]
                        then

                        echo "$(DATETIME) $PID [$FILENAME] ERROR: The sql loader call failed." >> $LOGFILE
                        #        Logmessage "$File_name" "$Log_Type1" "The sql loader call failed."
                                rm -f $WRKDIR$TEMP_FILE $WRKDIR$TEMP_FILE1 $WRKDIR$WRKFILE

                                exit 1
                        fi

COUNT_DUP=`cat $DUPDIR$DUPFILE | wc -l`
COUNT_REC=`cat $WRKDIR$WRKFILE | wc -l`
COUNT_UNPRCESSED=`cat $HOLDDIR$BADFILE | wc -l`

echo "$(DATETIME) $PID [$FILENAME] INFO: Running Procedure to load data into target tables" >>$LOGFILE

PRCINFO=`sqlplus -s $SQLPLUSCONN <<EOF
set serveroutput on
set termout off
set feedback off

declare

OUTPUT_STATUS number(2);

begin

PS_PHARMACY_PRC(OUTPUT_STATUS);

end;
/
EOF`


SQL_ARG1=`echo $PRCINFO | cut -d '|' -f1 | sed 's/\"//'`
SQL_ARG2=`echo $PRCINFO | cut -d '|' -f2 | sed 's/\"//'`
SQL_ARG3=`echo $PRCINFO | cut -d '|' -f3 | sed 's/\"//'`


        if [[ $SQL_ARG1 -eq 0 ]];
        then

                ARG1=`echo $SQL_ARG2 | cut -d ',' -f1 | sed 's/\"//'`
                ARG2=`echo $SQL_ARG2 | cut -d ',' -f2 | sed 's/\"//'`

                if [[ $ARG1 -eq 1 ]];
                then

                        echo "$(DATETIME) $PID [$FILENAME] INFO: $ARG2" >> $LOGFILE
                fi

                ARG3=`echo $SQL_ARG3 | cut -d ',' -f1 | sed 's/\"//'`
                ARG4=`echo $SQL_ARG3 | cut -d ',' -f2 | sed 's/\"//'`

                if [[ $ARG3 -eq 1 ]];
                then

                        echo "$(DATETIME) $PID [$FILENAME] INFO: $ARG4" >> $LOGFILE
                fi

#               mv -f $INDIR$FILENAME $PROCESSED
        elif [[ $SQL_ARG1 -eq 1 ]];
        then
                mv -f $INDIR$FILENAME $ERROR
                echo "$(DATETIME) $PID [$FILENAME] ERROR:" $SQL_ARG2 >> $LOGFILE

                if [[ -e $WRKDIR$WRKFILE && -e $WRKDIR$TEMP_FILE && -e $WRKDIR$TEMP_FILE1 ]]; then

                       rm -f $WRKDIR$WRKFILE
                       rm -rf $WRKDIR$TEMP_FILE $WRKDIR$TEMP_FILE1
                fi

                continue
        fi

echo "$(DATETIME) $PID [$FILENAME] INFO: Procedure successfully completed" >> $LOGFILE
echo "$(DATETIME) $PID [$FILENAME] INFO: File $FILENAME successfully processed; Total Transactions=$COUNT_REC; Duplicates Dropped=$COUNT_DUP" >> $LOGFILE

if [[ $COUNT_DUP -eq 0 ]]; then
rm -f $DUPDIR$DUPFILE
fi



if [[ $COUNT_UNPRCESSED -eq 0 ]]; then
rm -f $HOLDDIR$BADFILE
fi

mv -f $INDIR$FILENAME $PROCESSED$DESTFILE

if [[ -e $WRKDIR$WRKFILE && -e $WRKDIR$TEMP_FILE && -e $WRKDIR$TEMP_FILE1 ]]; then

        rm -f $WRKDIR$WRKFILE
        rm -rf $WRKDIR$TEMP_FILE $WRKDIR$TEMP_FILE1
fi

DEST_FILE_COUNT=`cat $PROCESSED$DESTFILE | wc -l`

if [[ $DEST_FILE_COUNT -eq 0 ]]; then
rm -f $PROCESSED$DESTFILE
fi
done
exit 0
#!/bin/bash

# Script Name: uk_lrx_cohort_cset_to_load.sh
# Release Version : 1.0
# Release Date : 15-10-2015
# Developed by Himanshu Ganatra
#
# This script is used to process the CSET file received from LO or DDMS team and forward the output to Bureau.

cd '/data/production/containers/dcp-prod/uk/uk_lrx/scripts/'
source '../config/uk_lrx.conf'

INDIR=$IN"uk_lrx_cset_to_dcp/"
INFILE="CSET*"
TEMPFILE=$WRKDIR"Temp$$"
TEMPFILE1=$WRKDIR"Temp1$$"
TEMPFILE2=$WRKDIR"Temp2$$"
LOGFILE=$LOGDIR"CSET_log.log"
CTLFILE=$CONFIG"load_cohorts.ctl"
DATE=`date +%Y%m%d`
LOGDATE=`date +%Y-%m-%d`
TIME=`date +%H:%M:%S`
PID=$$
CHECKDIR=`ls -1 $INDIR | wc -l`

DATETIME () {
date "+%Y-%m-%d %H:%M:%S";
}


if [ $CHECKDIR -eq 0 ]; then
echo "$(DATETIME) ERROR: No files to process in $INDIR" >> $LOGFILE
exit 1
fi

for i in `ls $INDIR$INFILE`
do
	FILENAME=$(basename "$i")
	FILENAME_WE=${FILENAME%.*} 			   #Filename wihtout extension
	WRKFILE=$FILENAME_WE".wrk"
	CTLLOGFILE=$LOGDIR$FILENAME_WE"_ctl.log"
	UNPROCESSED_FILE=$FILENAME_WE"_unprocessed"
	COHORT_SET=`echo $FILENAME | cut -d '_' -f1 | sed "s/[^0-9]//g"`
	CLIENT_NUMBER=`echo $COHORT_SET | cut -c1-3`
	PROJECT_NUMBER=`echo $COHORT_SET | cut -c4-5`
	FILEDATE=`echo $FILENAME | cut -d '_' -f2 | sed "s/[^0-9]//g"`
	OUT_BUREAU="CREQ"$COHORT_SET"_"$FILEDATE".dat"
	LOGHEADER="$LOGDATE $TIME $PID [$FILENAME]"

	if [ $COHORT_SET -eq '00000' ]; then
		echo "$(DATETIME) $PID [$FILENAME] INFO: Processing DDMS File $FILENAME" >> $LOGFILE
		DDMS=1
		touch $TEMPFILE
		chmod 777 $TEMPFILE
		cat $INDIR$FILENAME | sed 's/\$/"/g' | sed '1d' > $TEMPFILE1
                cat "$TEMPFILE1" | sed '$d' > $TEMPFILE
		cat $TEMPFILE | perl -F',(?=(?:(?:[^\"]*\"){2})*[^\"]*$)' -lane 'print $F[20],",DUMMY",$F[49]' | sed -e 's/"/,"/' > $TEMPFILE2
		cat $TEMPFILE2 | sed -e 's/"//g' > $WRKDIR$WRKFILE
	else
		echo "$(DATETIME) $PID [$FILENAME] INFO: Processing CSET File $FILENAME" >> $LOGFILE
		DDMS=0
		cp $INDIR$FILENAME $WRKDIR$WRKFILE
	fi

echo "$(DATETIME) $PID [$FILENAME] INFO: Creating Table in Database" >> $LOGFILE
echo "Creating Dynamic Table"

sqlplus -silent $SQLLDRCONN << EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
set serveroutput ON

exec CSET_CHECKTABLEEXIST('$FILENAME_WE');
exit;
EOF

echo "Error code $?"

if [ $? -ne 0 ]; then
	echo "$(DATETIME) $PID [$FILENAME] ERROR: Procedure Failed for creating a dynamic table for $FILENAME" >> $LOGFILE
	echo "$(DATETIME) $PID [$FILENAME] ERROR: Unable to process $FILENAME" >> $LOGFILE
	continue
else 
	echo "$(DATETIME) $PID [$FILENAME] INFO: Procedure to create a dynamic table for $FILENAME ran successfully" >>$LOGFILE
fi

#Creating Control file.
	echo "load data" > $CTLFILE
	echo "into table $FILENAME_WE" >> $CTLFILE
	echo "fields terminated by \",\" optionally enclosed by '\"'" >> $CTLFILE
	echo "TRAILING NULLCOLS" >> $CTLFILE
	echo "(PRESCRIBER_ID, PCO_DESCRIPTION,COHORT_ID)" >> $CTLFILE

sqlldr $SQLLDRCONN control=$CTLFILE data=$WRKDIR$WRKFILE log=$CTLLOGFILE bad=$ERROR$UNPROCESSED_FILE >/dev/null 2>&1

if [ $? -ne 0 ]; then
	echo "$(DATETIME) $PID [$FILENAME] ERROR: Data Loading Failed, please check Control Log for more details" >> $LOGFILE
	cp $INDIR$FILENAME $ERROR$FILENAME
	rm -f $INDIR$FILENAME
	rm -f $WRKDIR$WRKFILE
sqlplus -s $SQLPLUSCONN >/dev/null 2>&1 <<EOF
drop table $FILENAME_WE;
exit;
EOF
echo "$(DATETIME) $PID [$FILENAME] ERROR: Unable to process $FILENAME" >> $LOGFILE
	continue
fi

if [ $DDMS -eq 0 ]; then
echo "$(DATETIME) $PID [$FILENAME] INFO: Validating $FILENAME" >> $LOGFILE
	VALIDATION=`sqlplus -s $SQLPLUSCONN <<EOF
	SET SERVEROUTPUT ON
	SET FEED OFF
	WHENEVER OSERROR EXIT 9;
	WHENEVER SQLERROR EXIT SQL.SQLCODE;
	exec CSET_FILE_VALIDATION ('$FILENAME_WE');
	exit;
	EOF`
	
VALIDATION=`echo "$VALIDATION" | tr '\n' '|' | sed 's/|$/"/g' | sed 's/^/"/g'`

	if [[ $VALIDATION = *"FAILED"* ]]
	then
		echo "$(DATETIME) $PID [$FILENAME] ERROR: $VALIDATION" | sed  's/s\sF/s\nF/g' >> $LOGFILE 
sqlplus -s $SQLPLUSCONN >/dev/null 2>&1 <<EOF
drop table $FILENAME_WE;
exit;
EOF
		cp $INDIR$FILENAME $ERROR$FILENAME
		rm -f $INDIR$FILENAME
		rm -f $WRKDIR$WRKFILE
		echo "$LOGHEADER ERROR: Unable to process $FILENAME" >> $LOGFILE
		continue
	fi

echo "$(DATETIME) $PID [$FILENAME] INFO: File Successfully Validated" >> $LOGFILE
echo "$(DATETIME) $PID [$FILENAME] INFO: Generating CREQ File for BUREAU" >> $LOGFILE
	
sqlplus -s $SQLPLUSCONN >/dev/null 2>&1 <<EOF
SET linesize 2500 trims on pages 0 feed off
SET TRIMSPOOL ON
SET TRIMOUT ON
SET TERMOUT OFF
SPOOL $BUREAU_OUT$OUT_BUREAU

WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;

select prescriber_id||','||cohort_id from $FILENAME_WE;
SPOOL OFF

insert into PS_FILE values (SRC_FILE_ID_SEQ.NEXTVAL, '$OUT_BUREAU', sysdate, '1');
commit;
drop table $FILENAME_WE;
exit;
EOF

echo "$LOGHEADER INFO: File $OUT_BUREAU Successfully Generated" >> $LOGHEADER

cp $INDIR$FILENAME $PROCESSED$FILENAME
rm -f $INDIR$FILENAME
rm -f $WRKDIR$WRKFILE

elif [ $DDMS -eq 1 ]; then 

echo "$(DATETIME) $PID [$FILENAME] INFO: Running procedure for creating Cohort IDs" >> $LOGFILE

sqlplus -s $SQLPLUSCONN  << EOF
exec PS_COHT_GEN_PRC ('$FILENAME_WE');
exit;
EOF

echo "$(DATETIME) $PID [$FILENAME] INFO: Generating Output for $FILENAME" >> $LOGFILE

sqlplus -s $SQLPLUSCONN >/dev/null 2>&1 <<EOF
SET linesize 2500 trims on pages 0 feed off
SET TRIMSPOOL ON
SET TRIMOUT ON
SET TERMOUT OFF
SPOOL $BUREAU_OUT$OUT_BUREAU

WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
select distinct A.PRESCRIBER_ID ||','|| B.COHORT_ID from $FILENAME_WE A inner join PS_COHT_GEN B on A.COHORT_ID=B.EXTRNL_BRK_ID where A.PRESCRIBER_ID IS NOT NULL;

SPOOL OFF

insert into PS_FILE values (SRC_FILE_ID_SEQ.NEXTVAL, '$OUT_BUREAU', sysdate, '1','$OUT_BUREAU');
commit;
drop table $FILENAME_WE;
exit;
EOF

if [ $? -ne 0 ]; then
	echo "$(DATETIME) $PID [$FILENAME] ERROR: Unable to Generate Output file for $FILENAME" >> $LOGFILE
	sqlplus -s $SQLPLUSCONN >/dev/null 2>&1 <<EOF
drop table $FILENAME_WE;
exit;
EOF
		cp $INDIR$FILENAME $ERROR$FILENAME
		rm -f $INDIR$FILENAME
		rm -f $WRKDIR$WRKFILE
		rm -f $TEMPFILE
		rm -f $TEMPFILE1
		rm -f $TEMPFILE2
		echo "$(DATETIME) $PID [$FILENAME] ERROR: Unable to process $FILENAME" >> $LOGFILE
		continue
	fi
	

echo "$(DATETIME) $PID [$FILENAME] INFO: File $OUT_BUREAU Successfully Created" >> $LOGFILE
cp $INDIR$FILENAME $PROCESSED$FILENAME
	rm -f $INDIR$FILENAME
	rm -f $WRKDIR$WRKFILE
	rm -f $TEMPFILE
	rm -f $TEMPFILE1
	rm -f $TEMPFILE2
fi
done

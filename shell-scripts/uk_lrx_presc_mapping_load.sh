#!/bin/bash

# Script Name: uk_lrx_presc_mapping_to_load.sh
# Release Version : 1.0
# Release Date : 15-10-2015
# Developed by Himanshu Ganatra
#
# This script is used to load the data from presc_mapping file to database.


cd '/data/production/containers/dcp-prod/uk/uk_lrx/scripts/'
source '../config/uk_lrx.conf'

INDIR=$IN"uk_lrx_presc_mapping_to_dcp/"
INFILE="presc_map*"
LOGFILE=$LOGDIR"uk_lrx_prescriber_mapping_DCP_load.log"
CTLFILE=$CONFIG"load_presc_map.ctl"

PID=$$
CHECKDIR=`ls -1 $INDIR | wc -l`

DATETIME () {
date "+%Y-%m-%d %H:%M:%S";
}

#If not files exist in INDIR
if [ $CHECKDIR -eq 0 ]; then
echo "$(DATETIME) $PID ERROR: No files to process in $INDIR" >> $LOGFILE
exit 1
fi

for i in `ls $INDIR$INFILE`
{
FILENAME=$(basename "$i")
BADFILE=${FILENAME%.*}_unprocessed.dat
UNIQUE=${FILENAME%.*}_unique.dat
DUPFILE=$FILENAME"_duplicates"
CTLLOGFILE=$LOGDIR$FILENAME"_ctl.log"

echo "Processing $FILENAME"
echo "$(DATETIME) $PID [$FILENAME] INFO: Input file: $i" >> $LOGFILE

awk 'a[$0]++' $i > $DUPDIR$DUPFILE   	#To seperate out the duplicate records
awk '!a[$0]++' $i > $WRKDIR$UNIQUE	#To remove duplicate records from same file.

#Inserting file information to PS_FILE and truncating the Staging table.

sqlplus $SQLPLUSCONN >$NULL 2>&1 <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_MAP_FILE_INFO('$FILENAME');
exit;
EOF

# If error occurs while running the Procedure.
if [ $? -ne 0 ]; then
echo "$(DATETIME) $PID [$FILENAME] ERROR: Error while inserting data into PS_FILE or Truncating PS_ANON_RXER_MAP_STG" >>$LOGFILE
exit 1
fi

FILE_ID=`sqlplus -silent $SQLPLUSCONN <<EOF
select max(SRC_FILE_ID) from PS_FILE;
EOF`

FILE_ID=$(sed "s/[^0-9]//g" <<<$FILE_ID)

#loading the data using SQLLDR
sqlldr $SQLLDRCONN control=$CTLFILE data=$WRKDIR$UNIQUE log=$CTLLOGFILE bad=$ERROR$BADFILE >/dev/null 2>&1

COUNT_DUP=`cat $DUPDIR$DUPFILE | wc -l`
COUNT_REC=`cat $i | wc -l`

echo "$(DATETIME) $PID [$FILENAME] INFO: Running Procedure to load data into target tables" >>$LOGFILE
echo "Running Procedure for $FILENAME"

PRCINFO=`sqlplus -s $SQLPLUSCONN >>$LOGFILE <<EOF
WHENEVER OSERROR EXIT 9;
WHENEVER SQLERROR EXIT SQL.SQLCODE;
execute PS_RXER_MAP_PRC($FILE_ID);
exit;
EOF`

RESULT=$?

PRCINFO=`echo "$PRCINFO" | tr '\n' '|' |sed 's/|/ | /g'| sed 's/ | $/"/g' | sed 's/^/"/g'`

if [ $RESULT -ne 0 ]; then
echo "$(DATETIME) $PID [$FILENAME] ERROR: Procedure failed for $FILENAME" >> $LOGFILE
echo "$(DATETIME) $PID [$FILENAME] ERROR: File Moved to Error Directory" >> $LOGFILE
mv $INDIR$FILENAME $ERROR
rm $WRKDIR$UNIQUE
continue
else
echo "$(DATETIME) $PID [$FILENAME] INFO: Procedure Ran successfully for $FILENAME" >> $LOGFILE
fi

echo "$(DATETIME) $PID [$FILENAME] INFO: Procedure successfully completed" >> $LOGFILE
echo "$(DATETIME) $PID [$FILENAME] INFO: File $FILENAME successfully processed; Total Transactions=$COUNT_REC; Duplicates Dropped=$COUNT_DUP" >> $LOGFILE

if [ -s $DUPDIR$DUPFILE ]; then
rm -f $DUPDIR$DUPFILE
fi

mv $INDIR$FILENAME $PROCESSED
}
